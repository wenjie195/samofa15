<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allUser = getUser($conn, " WHERE user_type = ? ", array("user_type"), array(1), "s");
$allWithdrawal = getWithdrawal($conn, " WHERE status = ? ", array("status"), array("PENDING"), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="Admin Dashboard | Samofa 莎魔髪" />
    <title>Admin Dashboard | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_DASHBOARD ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding">
		<div class="four-div dashboard-box">
        	<img src="img/icons1.png" alt="<?php echo _ADMIN_CURRENT_ORDER ?>" title="<?php echo _ADMIN_CURRENT_ORDER ?>">
            <p class="box-p first-p1"><b><?php echo _ADMIN_CURRENT_ORDER ?></b></p>
            <p class="box-p">20</p>
        </div>
        <a href="adminViewWithdrawal.php">
            <div class="four-div dashboard-box second-four-div opacity-hover pointer four-mid">
                <img src="img/icon6.png" alt="<?php echo _ADMIN_WITHDRAWAL_REQUEST ?>" title="<?php echo _ADMIN_WITHDRAWAL_REQUEST ?>">
                <p class="box-p first-p1"><b><?php echo _ADMIN_WITHDRAWAL_REQUEST ?></b></p>
                <?php
                if($allWithdrawal)
                {   
                    $totalWithdrawal = count($allWithdrawal);
                }
                else
                {   $totalWithdrawal = 0;   }
                ?>
                <!-- <p class="box-p">5</p> -->
                <p class="box-p"><?php echo $totalWithdrawal;?></p>
            </div>   
        </a>     
		<div class="four-div dashboard-box third-four-div">
        	<img src="img/icons3.png" alt="<?php echo _ADMIN_NEW_REGISTRATION ?>" title="<?php echo _ADMIN_NEW_REGISTRATION ?>">
            <p class="box-p first-p1"><b><?php echo _ADMIN_NEW_REGISTRATION ?></b></p>
            <p class="box-p">3</p>
        </div>     
        <a href="adminAllMember.php">
             <div class="four-div dashboard-box forth-div opacity-hover pointer">
                <img src="img/icon3.png" alt="<?php echo _ADMIN_TOTAL_MEMBERS ?>" title="<?php echo _ADMIN_TOTAL_MEMBERS ?>">
                <p class="box-p first-p1"><b><?php echo _ADMIN_TOTAL_MEMBERS ?></b></p>
                <?php
                if($allUser)
                {   
                    $totalUser = count($allUser);
                }
                else
                {   $totalUser = 0;   }
                ?>
                <!-- <p class="box-p">500</p> -->
                <p class="box-p"><?php echo $totalUser;?></p>
            </div>        
        </a> 
        <a href="adminViewCurrentProduct.php">   
		<div class="four-div dashboard-box four-mid opacity-hover pointer">
        	<img src="img/icons5.png" alt="<?php echo _ADMIN_TOTAL_PACKAGE ?>" title="<?php echo _ADMIN_TOTAL_PACKAGE ?>">
            <p class="box-p second-p2"><b><?php echo _ADMIN_TOTAL_PACKAGE ?></b></p>
            <p class="box-p">3</p>
        </div>
        </a>
            <div class="four-div dashboard-box second-four-div">
                <img src="img/icon5.png" alt="<?php echo _ADMIN_TOTAL_REVENUE ?>" title="<?php echo _ADMIN_TOTAL_REVENUE ?>">
                <p class="box-p second-p2"><b><?php echo _ADMIN_TOTAL_REVENUE ?></b></p>
                <p class="box-p">RM500,000</p>
            </div>         
		<div class="four-div dashboard-box third-four-div">
        	<img src="img/icons2.png" alt="<?php echo _ADMIN_TOTAL_COMMISSION ?>" title="<?php echo _ADMIN_TOTAL_COMMISSION ?>">
            <p class="box-p second-p2 third-p3"><b><?php echo _ADMIN_TOTAL_COMMISSION ?></b></p>
            <p class="box-p">RM200,000</p>
        </div>    
        <a href="adminWithdrawalReport.php">       
		<div class="four-div dashboard-box four-mid forth-div opacity-hover pointer">
        	<img src="img/icons4.png" alt="<?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAWN ?>" title="<?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAWN ?>">
            <p class="box-p second-p2 third-p3"><b><?php echo _ADMIN_TOTAL_COMMISSION_WITHDRAWN ?></b></p>
            <p class="box-p">RM150,000</p>
        </div>
        </a>        
    </div>
</div>
<div class="clear"></div>
<div class="width100 same-padding">
        <p class="note-p eight-div-p opacity-hover pointer big-header-color"><a href="pdf/SAMOFA-2020-System-Marketing-Plan.pdf" class="big-header-color" target="_blank" download><?php echo _MILESTONE_SAMOFA_MARKETING ?><img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></a></p>
        <p class="note-p eight-div-p opacity-hover pointer big-header-color"><a href="pdf/price-list.pdf" class="big-header-color" target="_blank" download><?php echo _MILESTONE_PRODUCT_PRICE ?><img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></a></p> 
</div>
<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
