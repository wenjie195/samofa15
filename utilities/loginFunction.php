<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$conn->close();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

    $conn = connDB();

    if(isset($_POST['loginButton'])){

        $username = rewrite($_POST['username']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];

                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);

                if($finalPassword == $user->getPassword())
                {
                    // if(isset($_POST['remember-me']))
                    // {

                    //     // setcookie('email-oilxag', $email, time() + (86400 * 30), "/");
                    //     setcookie('username-oilxag', $email, time() + (86400 * 30), "/");
                    //     setcookie('password-oilxag', $password, time() + (86400 * 30), "/");
                    //     setcookie('remember-oilxag', 1, time() + (86400 * 30), "/");
                    //     // echo 'remember me';
                    // }
                    // else
                    // {
                    //     // setcookie('email-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('username-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('password-oilxag', '', time() + (86400 * 30), "/");
                    //     setcookie('remember-oilxag', 0, time() + (86400 * 30), "/");
                    //     // echo 'null';
                    // }

                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['usertype'] = $user->getUserType();
                    $_SESSION['login_type'] = $user->getLoginType();

                    if($user->getLoginType() == 1)
                    {

                        if($user->getUserType() == 0)
                        {
                            header('Location: ../adminDashboard.php');
                        }
                        // else
                        elseif($user->getUserType() == 1)
                        {
                            header('Location: ../userDashboard.php');
                        }
                        else
                        {
                            echo "invalid user type !";
                        }

                    }
                    else
                    {
                        header('Location: ../index.php?userDeactivated');
                    }

                }
                else
                {
                    echo "<script>alert('incorrect password');window.location='../index.php'</script>";
                }

        }
        else
        {
          echo "<script>alert('no user with this username');window.location='../index.php'</script>";
        }
    }

    $conn->close();
}
?>