<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $title = rewrite($_POST["update_title"]);
    $content = rewrite($_POST["update_content"]);


    $annouceUid = rewrite($_POST["announcement_uid"]);

    // // FOR DEBUGGING 
    // echo "<br>";
    // echo $username."<br>";
    // echo $firstname."<br>";

    $annouce = getAnnouncement($conn," uid = ? ",array("uid"),array($annouceUid),"s");    

    if(!$annouce)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($title)
        {
            array_push($tableName,"title");
            array_push($tableValue,$title);
            $stringType .=  "s";
        }
        if($content)
        {
            array_push($tableName,"content");
            array_push($tableValue,$content);
            $stringType .=  "s";
        }
        array_push($tableValue,$annouceUid);
        $stringType .=  "s";
        $annouceUpdated = updateDynamicData($conn,"announcement"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($annouceUpdated)
        {
            echo "<script>alert('Update Annoucement success !');window.location='../adminViewAnnouncement.php'</script>";
        }
        else
        {
            echo "<script>alert('Fail to update annoucement!');window.location='../adminViewAnnouncement.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../adminViewAnnouncement.php'</script>";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
