<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid  = $_SESSION['uid'];

     $editPassword_new  = $_POST['update_epin'];
     $userUid  = $_POST['user_uid'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userDetails = $user[0];
     $dbPass =  $userDetails->getEpin();
     $dbSalt =  $userDetails->getEpinSalt();

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $editPassword_current."<br>";
     // echo $editPassword_new."<br>";
     // echo $editPassword_reenter."<br>";

     if(strlen($editPassword_new) >= 6 )
     {
          $password = hash('sha256',$editPassword_new);
          $salt = substr(sha1(mt_rand()), 0, 100);
          $finalPassword = hash('sha256', $salt.$password);

          $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("epin","salt_epin"),array($finalPassword,$salt,$userUid),"sss");
          if($passwordUpdated)
          {
               // $_SESSION['messageType'] = 1;
               // header( "Location: ../profile.php");
               echo "<script>alert('Update E-Pin success !');window.location='../changeAddEPin.php'</script>";
          }
          else 
          {
               //echo "//server problem ";
               echo "<script>alert('Fail to update E-Pin !');window.location='../changeAddEPin.php'</script>";
          }
     }
     else 
     {
          // echo "password length must be more than 6 ";
          echo "<script>alert('E-Pin length must be more than 5');window.location='../changeAddEPin.php'</script>";
     }    
}
else
{
     header('Location: ../index.php');
}
?>