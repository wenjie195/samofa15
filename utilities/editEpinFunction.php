<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid  = $_SESSION['uid'];

     $editPassword_current  = $_POST['current_password'];
     $editPassword_new  = $_POST['new_password'];
     $editPassword_reenter  = $_POST['retype_new_password'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userDetails = $user[0];
     $dbPass =  $userDetails->getEpin();
     $dbSalt =  $userDetails->getEpinSalt();

     $editPassword_current_hashed = hash('sha256',$editPassword_current);
     $editPassword_current_hashed_salted = hash('sha256', $dbSalt . $editPassword_current_hashed);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $editPassword_current."<br>";
     // echo $editPassword_new."<br>";
     // echo $editPassword_reenter."<br>";

     if($editPassword_current_hashed_salted == $dbPass)
     {
          if(strlen($editPassword_new) >= 6 && strlen($editPassword_reenter) >= 6 )
          {
               if($editPassword_new == $editPassword_reenter)
               {
                    $password = hash('sha256',$editPassword_new);
                    $salt = substr(sha1(mt_rand()), 0, 100);
                    $finalPassword = hash('sha256', $salt.$password);

                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("epin","salt_epin"),array($finalPassword,$salt,$uid),"sss");
                    if($passwordUpdated)
                    {
                         echo "<script>alert('Update E-Pin success !');window.location='../addEpin.php'</script>";
                    }
                    else 
                    {
                         echo "<script>alert('Fail to update E-Pin !');window.location='../addEpin.php'</script>";
                    }
               }
               else 
               {
                    echo "<script>alert('E-Pin must be same with reenter E-Pin !!');window.location='../addEpin.php'</script>";
               }
          }
          else 
          {
               echo "<script>alert('E-Pin length must be more than 5');window.location='../addEpin.php'</script>";
          }
     }
     else 
     {
          echo "<script>alert('current E-Pin was wrong !!');window.location='../addEpin.php'</script>";
     }    
}
else
{
     header('Location: ../index.php');
}
?>