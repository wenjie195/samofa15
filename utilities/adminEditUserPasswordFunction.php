<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid  = $_SESSION['uid'];

     // $editPassword_current  = $_POST['update_password'];
     $editPassword_new  = $_POST['update_password'];
     $userUid  = $_POST['user_uid'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
     $userDetails = $user[0];
     $dbPass =  $userDetails->getPassword();
     $dbSalt =  $userDetails->getSalt();

     // $editPassword_current_hashed = hash('sha256',$editPassword_current);
     // $editPassword_current_hashed_salted = hash('sha256', $dbSalt . $editPassword_current_hashed);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $editPassword_current."<br>";
     // echo $editPassword_new."<br>";
     // echo $editPassword_reenter."<br>";

     if(strlen($editPassword_new) >= 6 )
     {
               $password = hash('sha256',$editPassword_new);
               $salt = substr(sha1(mt_rand()), 0, 100);
               $finalPassword = hash('sha256', $salt.$password);

               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$userUid),"sss");
               if($passwordUpdated)
               {
                    // $_SESSION['messageType'] = 1;
                    // header( "Location: ../profile.php");
                    echo "<script>alert('Update Password success !');window.location='../adminViewAllMembers.php'</script>";
               }
               else 
               {
                    //echo "//server problem ";
                    echo "<script>alert('Fail to update password !');window.location='../adminViewAllMembers.php'</script>";
               }
     }
     else 
     {
          // echo "password length must be more than 6 ";
          echo "<script>alert('password length must be more than 5');window.location='../adminViewAllMembers.php'</script>";
     }  
}
else
{
     header('Location: ../index.php');
}
?>