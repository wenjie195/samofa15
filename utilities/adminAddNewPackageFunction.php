<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$uid  = $_SESSION['uid'];

function addNewProduct($conn,$name,$price,$rankPrice,$rankAPrice,$rankBPrice,$rankCPrice,$description,$imageOne,$display,$type,$productType)
{
     if(insertDynamicData($conn,"product",array("name","price","rankOri","rankA","rankB","rankC","description","images","display","type","product_type"),
          array($name,$price,$rankPrice,$rankAPrice,$rankBPrice,$rankCPrice,$description,$imageOne,$display,$type,$productType),"ssssssssiis") === null)
     {
          // echo "gg";
     }
     else
     {    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid = md5(uniqid());

     $name = rewrite($_POST["product_name"]);
     $price = rewrite($_POST["product_price"]);
     $rankPrice = rewrite($_POST["product_member_price"]);

     $rankAPrice = rewrite($_POST["rank_a_price"]);
     $rankBPrice = rewrite($_POST["rank_b_price"]);
     $rankCPrice = rewrite($_POST["rank_c_price"]);

     $description = rewrite($_POST["product_description"]);

     $imageOne = $uid.$_FILES['image_one']['name'];
     $target_dir = "../ProductImages/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     // $title  = $_POST['title'];
     // $content  = $_POST['content'];
     $display  = "1";
     $type  = "1";
     $productType  = "Package";

     $allProduct = getProduct($conn," WHERE name = ? ",array("name"),array($_POST['product_name']),"s");
     $currentProductName = $allProduct[0];

     if (!$currentProductName)
     {
          if(addNewProduct($conn,$name,$price,$rankPrice,$rankAPrice,$rankBPrice,$rankCPrice,$description,$imageOne,$display,$type,$productType))
          { 
               echo "<script>alert('New Package Added !!');window.location='../adminViewCurrentProduct.php'</script>";  
          }
          else
          { 
               echo "<script>alert('fail to add new product !!');window.location='../adminAddNewPackage.php'</script>";  
          }
     }
     else
     {
          echo "<script>alert('Product name has been used, please insert a new product name !!');window.location='../adminAddNewPackage.php'</script>";  
     }

     // if(addNewProduct($conn,$name,$price,$rankPrice,$rankAPrice,$rankBPrice,$rankCPrice,$description,$imageOne,$display,$type,$productType))
     // { 
     //      echo "<script>alert('New Package Added !!');window.location='../adminViewCurrentProduct.php'</script>";  
     // }
     // else
     // { 
     //      echo "<script>alert('fail to add new product !!');window.location='../adminAddNewPackage.php'</script>";  
     // }

}
else
{
     header('Location: ../index.php');
}
?>