<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $withdrawalUid = md5(uniqid());

     $withdrawalUid = rewrite($_POST["withdrawal_uid"]);

     // $approvedStatus = rewrite($_POST["approved_status"]);
     // $rejectedStatus = rewrite($_POST["rejected_status"]);
     $withdrawalStatus = rewrite($_POST["withdrawal_status"]);

     $method = rewrite($_POST["method"]);
     $reference = rewrite($_POST["reference"]);
     $rejectedReason = rewrite($_POST["rejected_reason"]);

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $withdrawalUid."<br>";
     // // echo $approvedStatus."<br>";
     // // echo $rejectedStatus."<br>";
     // echo $method."<br>";
     // echo $reference."<br>";
     // echo $rejectedReason."<br>";

     if($withdrawalStatus == "APPROVED")
     {
          if(isset($_POST['withdrawal_uid']))
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               // //echo "save to database";

               if($withdrawalStatus)
               {
                    array_push($tableName,"status");
                    array_push($tableValue,$withdrawalStatus);
                    $stringType .=  "s";
               }
               if($method)
               {
                    array_push($tableName,"method");
                    array_push($tableValue,$method);
                    $stringType .=  "s";
               }
               if($reference)
               {
                    array_push($tableName,"note");
                    array_push($tableValue,$reference);
                    $stringType .=  "s";
               }
               array_push($tableValue,$withdrawalUid);
               $stringType .=  "s";
               $updateWithdrawalStatus = updateDynamicData($conn,"withdrawal"," WHERE withdrawal_uid = ? ",$tableName,$tableValue,$stringType);
               if($updateWithdrawalStatus)
               {
                    // echo "aprroved withdrawal success";
                    echo "<script>alert('withdrawal approved !!');window.location='../adminViewWithdrawal.php'</script>";
               }    
               else
               {
                    echo "fail";
               }
          }
          else
          {
               echo "error level 222";
          }
     }
     elseif($withdrawalStatus == "REJECTED")
     {
          $withdrawalDetails = getWithdrawal($conn," WHERE withdrawal_uid = ? ",array("withdrawal_uid"),array($withdrawalUid),"s");
          $withdrawalAmount =  $withdrawalDetails[0]->getAmount();
          $userUid =  $withdrawalDetails[0]->getUid();

          $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
          $userCurrentCredit =  $userDetails[0]->getAmount();

          $refundAmount = $withdrawalAmount + $userCurrentCredit;

          if(isset($_POST['withdrawal_uid']))
          {
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               // //echo "save to database";

               if($refundAmount)
               {
                    array_push($tableName,"amount");
                    array_push($tableValue,$refundAmount);
                    $stringType .=  "s";
               }
               array_push($tableValue,$userUid);
               $stringType .=  "s";
               $updateRejectWithdrawal = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateRejectWithdrawal)
               {
                    if(isset($_POST['withdrawal_uid']))
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         // //echo "save to database";
          
                         if($withdrawalStatus)
                         {
                              array_push($tableName,"status");
                              array_push($tableValue,$withdrawalStatus);
                              $stringType .=  "s";
                         }
                         if($rejectedReason)
                         {
                              array_push($tableName,"reason");
                              array_push($tableValue,$rejectedReason);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$withdrawalUid);
                         $stringType .=  "s";
                         $updateWithdrawalStatus = updateDynamicData($conn,"withdrawal"," WHERE withdrawal_uid = ? ",$tableName,$tableValue,$stringType);
                         if($updateWithdrawalStatus)
                         {
                              // echo "reject and refund success";
                              echo "<script>alert('withdrawal rejected and refunded !!');window.location='../adminViewWithdrawal.php'</script>";
                         }    
                         else
                         {
                              echo "fail";
                         }
                    }
                    else
                    {
                         echo "error level 2";
                    }
               }
               else
               {
                    echo "fail";
               }
          }
          else
          {
               echo "error level 1";
          }
     }  
     else
     {
          echo "unknown withdrawal status !!!";
     }
}
else
{
     header('Location: ../index.php');
}
?>