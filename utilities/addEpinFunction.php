<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();
    $uid  = $_SESSION['uid'];

    $register_epin = $_POST['current_password'];
    // $register_epin_validation = strlen($register_epin);
    $register_epin_reenter = $_POST['new_password'];

    $epin = hash('sha256',$register_epin);
    $salt1 = substr(sha1(mt_rand()), 0, 100);
    $finalePin = hash('sha256', $salt1.$epin);
    
    // echo $register_epin."<br>";
    // echo $salt1."<br>";
    // echo $finalePin."<br>";
    
    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");   

    if(strlen($register_epin) >= 6 && strlen($register_epin_reenter) >= 6 )
    {
        if($register_epin == $register_epin_reenter)
        {
            if(!$user)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($finalePin)
                {
                    array_push($tableName,"epin");
                    array_push($tableValue,$finalePin);
                    $stringType .=  "s";
                }
                if($salt1)
                {
                    array_push($tableName,"salt_epin");
                    array_push($tableValue,$salt1);
                    $stringType .=  "s";
                }
                
                array_push($tableValue,$uid);
                $stringType .=  "s";
                $addOnEpin = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($addOnEpin)
                {
                    echo "<script>alert('E-Pin Added !');window.location='../addEpin.php'</script>";
                }
                else
                {
                    echo "<script>alert('Fail to add e-pin !!');window.location='../addEpin.php'</script>";
                }
            }
            else
            {
                echo "<script>alert('ERROR !!');window.location='../addEpin.php'</script>";
            }
        }
        else 
        {
            echo "<script>alert('e-pin must be same with retype e-pin ');window.location='../addEpin.php'</script>";
        }

    }
    else 
    {
        echo "<script>alert('e-pin length must be more than 5');window.location='../addEpin.php'</script>";
    }
}
else
{
     header('Location: ../index.php');
}
?>