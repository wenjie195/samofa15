<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Cart.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid,$rank);
    header('Location: ./checkout.php');

}

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
// $rank= $userData->getStatus();
$rank= $userData->getRank();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <!-- <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" /> -->
    <meta property="og:title" content="View Cart | Samofa 莎魔髪" />
    <title>View Cart | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<?php include 'css.php'; ?> 
</head>

<body class="body">
    <?php include 'headerAfterLogin.php'; ?>
    <div class="width100 menu-distance75 min-height-with-menu-distance75">
	    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _VIEW_CART_CART ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

        <div class="overflow-scroll-div same-padding">
            <form method="POST">
                <table class="table-css">
                	<thead>
                        <tr>
                        <th><?php echo _PRODUCTDETAILS_IMAGE ?></th>
                            <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                    		
                            <th class="quantity-td"><?php echo _VIEW_CART_QUANTITY ?></th>
                            <th><?php echo _PRODUCTDETAILS_PRICE ?></th>
                            <th><?php echo _VIEW_CART_TOTAL ?></th>
                        </tr>
                    </thead>
                    
                <?php
                    $conn = connDB();
                    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                    {
                        $productListHtml = getShoppingCart($conn,$rank,2);
                        echo $productListHtml;
                    }
                    else
                    {
                        echo " <h3> YOUR CART IS EMPTY </h3>";
                    }


                    if(array_key_exists('xclearCart', $_POST))
                    {
                        xclearCart();
                    }
                    else
                    {
                    // code...
                        unset($productListHtml);
                    }

                    $conn->close();
                ?>

                <div class="width100 text-center">
                    <p onclick="goBack()" class="opacity-hover pink-text back-text"><img src="img/back.png" class="back-btn" alt="back" title="back"> <?php echo _VIEW_CART_BACK ?></p>
                </div>

            </form>
        
 
    </div>
    
    
    </div>


    <?php include 'js.php'; ?>
</body>
</html>