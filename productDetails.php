<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

//require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $getRank = getUser($conn, $_SESSION['uid'],false);

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    header('Location: ./viewCart.php');
}

//$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
// $rank= $userData->getStatus();
$rank= $userData->getRank();

$productDetails = getProduct($conn, "WHERE display='1' AND type='1' ");
// $productDetails = getProduct($conn, "WHERE display='1'  ");

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,$rank,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($productDetails,$rank,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($productDetails,$rank);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <!-- <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" /> -->
    <meta property="og:title" content="Product Details | Samofa 莎魔髪" />
    <title>Product Details | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
	<h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _PRODUCTDETAILS ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
    <!-- <form action="utilities/productOrderFunction.php" method="POST"> -->
    <!-- <form action="#" method="POST"> -->

    <!-- <h1 class="rank"><//?php echo $rank?> </h1> -->
        
    <form method="POST">
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                    <th><?php echo _PRODUCTDETAILS_IMAGE ?></th>
                    <th><?php echo _PRODUCTDETAILS_ORI_PRICE ?> (RM)</th>
                    <!-- <th><?php echo $rank ?><?php echo _PRODUCTDETAILS_DIS_PRICE ?> (RM)</th> -->
                    <!-- <th><?php echo _PRODUCTDETAILS_RANK_PRICE ?> (RM)</th> -->
                    <!-- <th><?php echo _PRODUCTDETAILS_STOCK ?></th> -->
                    <!-- <th><?php echo _PRODUCTDETAILS_STATUS ?></th> -->
                    <!-- <th><?php echo _PRODUCTDETAILS_DESCRIPTION ?></th> -->
                    <th><?php echo _VIEW_CART_QUANTITY ?></th>
                </tr>
            </thead>
            <tbody>
                <?php echo $productListHtml; ?>
            </tbody>
        </table>

        <div class="width100 text-center top-bottom-distance">
        	<button class="clean button-width transparent-button dark-pink-button" name="purchase"><?php echo _INDEX_ADD_TO_CART ?></button>
        </div>

    </form>
    </div>
</div>    

<div class="clear"></div>
<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

<!-- <script>
    function addToCart()
    {
        $shoppingCart = array();
        $totalProductCount = count($_POST['product-list-id-input']);
        for($i = 0; $i < $totalProductCount; $i++)
        {
            $productId = $_POST['product-list-id-input'][$i];
            $quantity = $_POST['product-list-quantity-input'][$i];
            $thisOrder = array();
            $thisOrder['productId'] = $productId;
            $thisOrder['quantity'] = $quantity;
            array_push($shoppingCart,$thisOrder);
        }
        if(count($shoppingCart) > 0)
        {
            $_SESSION['shoppingCart'] = $shoppingCart;
        }
    }
</script> -->

</body>
</html>