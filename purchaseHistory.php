<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$productsOrders =  getOrders($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <!-- <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" /> -->
    <meta property="og:title" content="Order History | Samofa 莎魔髪" />
    <title>Order History | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
	<h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _PURCHASE_ORDER_HISTORY ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
        
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                    <th><?php echo _INDEX_CONTACT_NO ?></th>
                    <th><?php echo _PRODUCTDETAILS_STATUS ?></th>
                    <th><?php echo _VIEW_CART_TOTAL ?> (RM)</th>
                    <th><?php echo _PURCHASE_DATE ?></th>
                    <th><?php echo _PURCHASE_DETAILS ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if($productsOrders)
                    {
                        for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $productsOrders[$cnt]->getUsername();?></td>
                                <td><?php echo $productsOrders[$cnt]->getContact();?></td>
                                <td><?php echo _PURCHASE_PENDING ?></td>
                                <td><?php echo $productsOrders[$cnt]->getTotal();?></td>
                                <td>
                                    <?php echo $date = date("d-m-Y",strtotime($productsOrders[$cnt]->getDateCreated()));?>
                                </td>
          
                                <td>
                                    <!-- <form method="POST" action="pendingPetDetails.php" class="hover1"> -->
                                    <form method="POST" action="#" class="hover1">
                                        <button class="clean hover1 img-btn transparent-button pink-hover-text" type="submit" name="order_id" value="<?php echo $productsOrders[$cnt]->getUid();?>">
                                            <?php echo _PURCHASE_VIEW ?>
                                        </button>
                                    </form>
                                </td>
                                
                            </tr>
                        <?php
                        }
                    }
                ?>   
            </tbody>
        </table>

    </div>
</div>    

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>