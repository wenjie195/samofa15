<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dxforextrade88.com/" />-->
    <meta property="og:title" content="Magic BloCA 脱糖宝 | Samofa 莎魔髪" />
    <title>Magic BloCA 脱糖宝 | Samofa 莎魔髪</title>
    <!--<link rel="canonical" href="https://dxforextrade88.com/" />-->
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="overflow width100 menu-distance min-height-with-menu-distance2">
    <div class="top-left-flower-div top-left-flower-div2">
        <img src="img/top-left-flower.png" class="top-left-flower" alt="Samofa 莎魔髪" title="Samofa 莎魔髪">
    </div>
    <div class="width100 overflow same-padding product-page-div">
    	<div class="left-product">
        	<img src="img/magic-bloca-product.png" class="product-page-product" alt="Magic BloCA 脱糖宝" title="Magic BloCA 脱糖宝">
        </div>
        <div class="right-product-content">
            <h2 class="product-title big-header-color ow-no-margin-top">Magic BloCA<sup class="big-header-color smaller-h1">TM</sup> 脱糖宝</h2>
            <h3 class="mild-pink-text h3-sub font-400"><?php echo _INDEX_MAGIC_SACHET ?></h3>
            <h3 class="dark-pink-text h3-sub middle-h3"><?php echo _INDEX_MAGIC_DESC ?></h3>
            
            <h3 class="mild-pink-text desc-h3"><?php echo _INDEX_MAGIC_DESC1 ?><br><br><?php echo _INDEX_MAGIC_DESC2 ?></h3>        	
         	<!-- Unhide it when you are ready for the add to cart function 
            <div class="width100 overflow product-quantity">
            	<p class="quantity-p dark-pink-text"><?php echo _INDEX_QUANTITY ?>:</p> <div class="numbers-row numbers-row-css"></div>
        	</div>
            <div class="border-btn add-to-cart-btn"><div class="white-bg"><?php echo _INDEX_ADD_TO_CART ?> | RM XXXX</div></div>  
            -->      
        </div>	
    </div>
    
    
    <div class="bottom-right-flower-div bottom-right-flower-div2">
        <img src="img/bottom-right-flower.png" class="bottom-right-flower" alt="Samofa 莎魔髪" title="Samofa 莎魔髪">
    </div>
    <div class="clear"></div>
    <div class="after-flower-info width100 same-padding">
    	<div class="row-div">
        	<div class="row-title-div option-heading">
            	
                <h3 class="big-header-color row-h3"><img src="img/benefit.png" alt="<?php echo _INDEX_FUNCTION ?>" class="row-img"> <?php echo _INDEX_FUNCTION ?></h3>
            </div>

            <div class="option-content is-hidden row-content">
            	<table class="transparent-table row-table">
                	<tbody>
                        <tr>
                            <td><img src="img/tick.png" alt="<?php echo _INDEX_FUNCTION ?>" class="td-img"></td>
                            <td><?php echo _INDEX_BLOCK_SUGAR ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/tick.png" alt="<?php echo _INDEX_FUNCTION ?>" class="td-img"></td>
                            <td><?php echo _INDEX_BLOCK_CARB ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/tick.png" alt="<?php echo _INDEX_FUNCTION ?>" class="td-img"></td>
                            <td><?php echo _INDEX_BLOCK_FAT_BURNING ?></td>
                        </tr> 
                        <tr>
                            <td><img src="img/tick.png" alt="<?php echo _INDEX_FUNCTION ?>" class="td-img"></td>
                            <td><?php echo _INDEX_BLOCK_DETOX ?></td>
                        </tr> 
                        <tr>
                            <td><img src="img/tick.png" alt="<?php echo _INDEX_FUNCTION ?>" class="td-img"></td>
                            <td><?php echo _INDEX_BODY_SLIM_DOWN ?></td>
                        </tr> 
                        <tr>
                            <td><img src="img/tick.png" alt="<?php echo _INDEX_FUNCTION ?>" class="td-img"></td>
                            <td><?php echo _INDEX_BODY_FIRM_UP ?></td>
                        </tr>  
                        <tr>
                            <td><img src="img/tick.png" alt="<?php echo _INDEX_FUNCTION ?>" class="td-img"></td>
                            <td><?php echo _INDEX_BODY_SHAPE ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/tick.png" alt="<?php echo _INDEX_FUNCTION ?>" class="td-img"></td>
                            <td><?php echo _INDEX_ANTI_HUNGER ?></td>
                        </tr>                
                       </tbody>                                                                                                                          
            	</table>
            </div>
		</div>
        <div class="row-div">
        	<div class="row-title-div option-heading">
            	
                <h3 class="big-header-color row-h3"><img src="img/arrow3.png" alt="<?php echo _INDEX_USE_METHOD ?>" class="row-img"> <?php echo _INDEX_USE_METHOD ?></h3>
            </div>

            <div class="option-content is-hidden row-content">
            	<table class="transparent-table row-table">
                	<tbody>
                        <tr>
                            <td><img src="img/drink.png" alt="<?php echo _INDEX_USE_METHOD ?>" class="td-img"></td>
                            <td><?php echo _INDEX_STEP1 ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/cut.png" alt="<?php echo _INDEX_USE_METHOD ?>" class="td-img"></td>
                            <td><?php echo _INDEX_STEP2 ?></td>
                        </tr>
                						
                    </tbody>                                                                                                                                          
            	</table>
            </div>            
        </div>    
    	<div class="row-div">
        	<div class="row-title-div option-heading">
            	
                <h3 class="big-header-color row-h3"><img src="img/ingredients2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="row-img"> <?php echo _INDEX_MAIN_INGREDIENTS ?></h3>
            </div>

            <div class="option-content is-hidden row-content">
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_MIX_FRUIT_POWDER ?></p>
            	<p class="inside-td-p mild-pink-text"><?php echo _INDEX_MIX_FRUIT_POWDER_DESC ?></p>
                
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_PSYLLIUM_HUSK ?></p>
            	<p class="inside-td-p mild-pink-text"><?php echo _INDEX_PSYLLIUM_HUSK_DESC ?></p>                
                
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_INULIN ?></p>
            	<p class="inside-td-p mild-pink-text"><?php echo _INDEX_INULIN_DESC ?></p>                 
                
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_WHITE_KIDNEY_BEAN ?></p>
            	<p class="inside-td-p mild-pink-text"><?php echo _INDEX_WHITE_KIDNEY_BEAN_DESC ?></p>                   
                
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_POLY ?></p>
            	<p class="inside-td-p mild-pink-text font-700 inside-td-p3"><img src="img/star.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png2"> <?php echo _INDEX_INSOLUBLE_DIETARY_FIBER ?></p> 
				<p class="inside-td-p inside-td-p2 mild-pink-text"><?php echo _INDEX_INSOLUBLE_DIETARY_FIBER_DESC ?></p> 
            	<p class="inside-td-p mild-pink-text font-700 inside-td-p3"><img src="img/star.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png2"> <?php echo _INDEX_WATER_SOLUBLE_DIETARY_FIBER ?></p> 
				<p class="inside-td-p inside-td-p2 mild-pink-text"><?php echo _INDEX_WATER_SOLUBLE_DIETARY_FIBER_DESC ?></p> 

            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_FLAVONE ?></p>
            	<p class="inside-td-p mild-pink-text"><?php echo _INDEX_FLAVONE_DESC ?></p>   

                
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_AMYLASE ?></p>
            	<table class="transparent-table row-table row-table2">
                	<tbody>
                        <tr>
                            <td><img src="img/star.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png2"></td>
                            <td><?php echo _INDEX_AMYLASE_DESC ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/star.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png2"></td>
                            <td><?php echo _INDEX_AMYLASE_DESC2 ?></td>
                        </tr>
                						
                    </tbody>                                                                                                                                          
            	</table>                 
                
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_MULTIENZYME ?></p>
            	<p class="inside-td-p mild-pink-text"><?php echo _INDEX_MULTIENZYME_DESC ?></p>     
                
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_KONJAC ?></p>
            	<p class="inside-td-p mild-pink-text"><?php echo _INDEX_KONJAC_DESC ?></p> 
                
            	<p class="td-p dark-pink-text"><img src="img/star2.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png"> <?php echo _INDEX_ALOE_VERA ?></p>
            	<table class="transparent-table row-table row-table2">
                	<tbody>
                        <tr>
                            <td><img src="img/star.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png2"></td>
                            <td><?php echo _INDEX_ALOE_VERA_DESC ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/star.png" alt="<?php echo _INDEX_MAIN_INGREDIENTS ?>" class="td-img star-png2"></td>
                            <td><?php echo _INDEX_ALOE_VERA_DESC2 ?></td>
                        </tr>
                						
                    </tbody>                                                                                                                                          
            	</table>                               
                               
            </div>
		</div>            
        <div class="row-div last-row">
        	<div class="row-title-div option-heading">
            	
                <h3 class="big-header-color row-h3"><img src="img/note.png" alt="<?php echo _INDEX_EXTRA_INFO ?>" class="row-img"> <?php echo _INDEX_EXTRA_INFO ?></h3>
            </div>

            <div class="option-content is-hidden row-content">
            	<table class="transparent-table row-table">
                	<tbody>
                        <tr>
                            <td><img src="img/star3.png" alt="<?php echo _INDEX_USE_METHOD ?>" class="td-img"></td>
                            <td><?php echo _INDEX_EXTRA1 ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/star3.png" alt="<?php echo _INDEX_USE_METHOD ?>" class="td-img"></td>
                            <td><?php echo _INDEX_EXTRA2 ?></td>
                        </tr>
                        <tr>
                            <td><img src="img/star3.png" alt="<?php echo _INDEX_USE_METHOD ?>" class="td-img"></td>
                            <td><?php echo _INDEX_EXTRA3 ?></td>
                        </tr>                						
                    </tbody>                                                                                                                                          
            	</table>
            </div>            
        </div>    


            
        </div>
    
    </div>
</div>
<?php include 'js.php'; ?>
</body>
</html>