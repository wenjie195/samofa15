<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];
$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$userLevel = $userRHDetails[0];

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="My Team | Samofa 莎魔髪" />
    <title>My Team | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title text-center modal-h1 big-header-color margin-top40"><?php echo _MYTEAM ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <?php $level = $userLevel->getCurrentLevel();?>

    <div class="overflow-scroll-div same-padding">
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _USERDASHBOARD_GEN ?></th>
                    <th><?php echo _JS_USERNAME ?></th>
                    <th>ID</th>
                    <th><?php echo _USERDASHBOARD_SPONSOR ?></th>
                    <th><?php echo _USERDASHBOARD_LAST_ORDER ?></th>
                    <th><?php echo _USERDASHBOARD_RANK ?></th>
                    <th><?php echo _JS_COUNTRY ?></th>
                    <th><?php echo _USERDASHBOARD_STATUS ?></th>
                    <th><?php echo _ADMIN_PERSONAL_SALES ?></th>
                    <th><?php echo _USERDASHBOARD_HIERARCHY ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($getWho)
                {
                    for($cnt = 0;$cnt < count($getWho) ;$cnt++)
                    {
                    ?>
                    <tr>
                        <td>
                            <?php 
                                $downlineLvl = $getWho[$cnt]->getCurrentLevel();
                                echo $lvl = $downlineLvl - $level;
                            ?>
                        </td>

                        <td>
                            <?php
                                $userUid = $getWho[$cnt]->getReferralId();

                                $thisUserDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
                                echo $username = $thisUserDetails[0]->getUsername();
                            ?>
                        </td>

                        <td><?php echo $userMemberID = $thisUserDetails[0]->getMemberID();?></td>

                        <td>
                            <?php
                                $uplineUid = $getWho[$cnt]->getReferrerId();

                                $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                                // $userData = $uplineDetails[0];

                                echo $uplineUsername = $uplineDetails[0]->getUsername();

                            ?>
                        </td>

                        <td><?php echo date("d-m-Y",strtotime($thisUserDetails[0]->getDateUpdated()));?></td>

                        <td>
                            <?php 
                                $rank = $thisUserDetails[0]->getRank();
                                if($rank != '')
                                {
                                    echo $rank;
                                }
                                else
                                {
                                    echo "non member";
                                }
                            ?>
                        </td>
                        <td><?php echo $country = $thisUserDetails[0]->getCountry();?></td>
                        <td><?php echo $status = $thisUserDetails[0]->getStatus();?></td>
                        <td><?php echo $salesValue = $thisUserDetails[0]->getSalesValue();?></td>
                        <!-- <td>Tree View</td> -->

                        <td>
                            <form action="userDownlineDetails.php" method="POST">
                                <button class="clean pink-button view-btn" type="submit" name="user_uid" value="<?php echo $getWho[$cnt]->getReferralId();?>"><?php echo _USERDASHBOARD_VIEW ?>

                                </button>
                            </form>
                        </td>

                    </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>

    <div class="clear"></div>
   </div>
<div class="clear"></div>
<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
