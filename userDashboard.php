<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$userLevel = $userRHDetails[0];

$withdrawalAmount = getWithdrawal($conn, " WHERE uid = ? AND status = 'APPROVED' ", array("uid"), array($uid), "s");

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
if ($getWho)
{
    $groupMember = count($getWho);
}
else
{
    $groupMember = 0;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="User Dashboard | Samofa 莎魔髪" />
    <title>User Dashboard | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _USERDASHBOARD_PERSONAL_MILESTONES ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="width100 same-padding container-div1">
    	<!-- if achieved, add the class achieved1 besides eight-div-->
		<div class="eight-div achieved1">
        	<div class="standard-div">
            	RM500
            </div>
            <div class="circle-line01"></div>
            <div class="circle01">1</div>
            
            <p class="eight-div-p">
            	<b><?php echo _MILESTONE_BONUS1 ?></b><br>
                <?php echo _MILESTONE_BONUS1_DESC1 ?><br><?php echo _MILESTONE_BONUS1_DESC2 ?>
            </p>
        </div> 
		<div class="eight-div achieved1">
        	<div class="standard-div">
            	RM1,000
            </div>
            <div class="circle-line01"></div>
            <div class="circle01">2</div>
            
            <p class="eight-div-p">
            	<b><?php echo _MILESTONE_BONUS2 ?></b><br>
                <?php echo _MILESTONE_BONUS2_DESC ?>
            </p>
        </div>        
		<div class="eight-div">
        	<div class="standard-div">
            	RM2,000
            </div>
            <div class="circle-line01"></div>
            <div class="circle01">3</div>
            
            <p class="eight-div-p">
            	<b><?php echo _MILESTONE_BONUS3 ?></b><br>
                <?php echo _MILESTONE_BONUS3_DESC ?>
            </p>
        </div>        
		<div class="eight-div">
        	<div class="standard-div">
            	RM3,500
            </div>
            <div class="circle-line01"></div>
            <div class="circle01">4</div>
            
            <p class="eight-div-p">
            	<b><?php echo _MILESTONE_BONUS4 ?></b><br>
                <?php echo _MILESTONE_BONUS4_DESC ?>
            </p>
        </div>        
		<div class="eight-div">
        	<div class="standard-div">
            	RM5,000
            </div>
            <div class="circle-line01"></div>
            <div class="circle01">5</div>
            
            <p class="eight-div-p">
            	<b><?php echo _MILESTONE_BONUS5 ?></b><br>
                <?php echo _MILESTONE_BONUS5_DESC ?>
            </p>
        </div>
		<div class="eight-div">
        	<div class="standard-div">
            	RM6,000
            </div>
            <div class="circle-line01"></div>
            <div class="circle01">6</div>
            
            <p class="eight-div-p">
            	<b><?php echo _MILESTONE_BONUS6 ?></b><br>
                <?php echo _MILESTONE_BONUS6_DESC ?>
            </p>
        </div>
		<div class="eight-div">
        	<div class="standard-div">
            	RM8,000
            </div>
            <div class="circle-line01"></div>
            <div class="circle01">7</div>
            
            <p class="eight-div-p">
            	<b><?php echo _MILESTONE_BONUS7 ?></b><br>
                <?php echo _MILESTONE_BONUS7_DESC ?>
            </p>
        </div>
		<div class="eight-div">
        	<div class="standard-div">
            	0.5%<?php echo _MILESTONE_GROUP_SALES ?>
            </div>
            <div class="circle-line01"></div>
            <div class="circle01">8</div>
            
            <p class="eight-div-p">
            	<b><?php echo _MILESTONE_BONUS8 ?></b><br>
                <?php echo _MILESTONE_BONUS8_DESC ?>
            </p>
        </div>
        <div class="clear"></div>
		<p class="note-p eight-div-p"><?php echo _MILESTONE_BONUS8_DESC2 ?></p> 
        <p class="note-p eight-div-p opacity-hover pointer big-header-color"><a href="pdf/SAMOFA-2020-System-Marketing-Plan.pdf" class="big-header-color" target="_blank" download><?php echo _MILESTONE_SAMOFA_MARKETING ?><img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></a></p>
        <p class="note-p eight-div-p opacity-hover pointer big-header-color"><a href="pdf/price-list.pdf" class="big-header-color" target="_blank" download><?php echo _MILESTONE_PRODUCT_PRICE ?><img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></a></p>                                                        
    </div>
    
    <div class="clear"></div>
   	
    <div class="width100 overflow same-padding dashboard-big-div">
    	<div class="three-div-width dashboard-box">
        	<img src="img/icon1.png" alt="<?php echo _USERDASHBOARD_PERFORMANCE ?>" title="<?php echo _USERDASHBOARD_PERFORMANCE ?>">
            <p class="box-p first-box-p second-first-p"><b><?php echo _USERDASHBOARD_PERFORMANCE ?></b></p>
            <p class="box-p">20 <?php echo _USERDASHBOARD_PACKAGES ?></p>
        </div>
    	<div class="three-div-width mid-three-div-width-margin dashboard-box second-third-box">
        	<img src="img/icon2.png" alt="<?php echo _USERDASHBOARD_GROUP_PERFORMANCE ?>" title="<?php echo _USERDASHBOARD_GROUP_PERFORMANCE ?>">
            <p class="box-p first-box-p second-first-p"><b><?php echo _USERDASHBOARD_GROUP_PERFORMANCE ?></b></p>
            <p class="box-p">100 <?php echo _USERDASHBOARD_PACKAGES ?></p>
        </div>    
        <a href="myTeam.php">
    	<div class="three-div-width dashboard-box opacity-hover pointer">
        	<img src="img/icon3.png" alt="<?php echo _USERDASHBOARD_MY_GROUP ?>" title="<?php echo _USERDASHBOARD_MY_GROUP ?>">
            <p class="box-p first-box-p third-first-p"><b><?php echo _USERDASHBOARD_MY_GROUP ?></b></p>
            <!-- <p class="box-p">50</p> -->
            <p class="box-p"><?php echo $groupMember ;?></p>
        </div> 
        </a>   
		<div class="tempo-three-clear2"></div>
    	<div class="three-div-width dashboard-box second-third-box">
        	<img src="img/icon4.png" alt="<?php echo _USERDASHBOARD_TOTAL_COMMISSION ?>" title="<?php echo _USERDASHBOARD_TOTAL_COMMISSION ?>">
            <p class="box-p third-first-p"><b><?php echo _USERDASHBOARD_TOTAL_COMMISSION ?></b></p>
            <p class="box-p">RM300</p>
        </div>
        <a href="withdrawalReport.php">
    	<div class="three-div-width mid-three-div-width-margin dashboard-box opacity-hover pointer">
        	<img src="img/icon5.png" alt="<?php echo _USERDASHBOARD_TOTAL_WITHDRAWAL ?>" title="<?php echo _USERDASHBOARD_TOTAL_WITHDRAWAL ?>">
            <p class="box-p third-first-p"><b><?php echo _USERDASHBOARD_TOTAL_WITHDRAWAL ?></b></p>
            <?php
            if($withdrawalAmount)
            {
                $totalAmount = 0;
                for ($cnt=0; $cnt <count($withdrawalAmount) ; $cnt++)
                {
                    $totalAmount += $withdrawalAmount[$cnt]->getAmount();
                }
            }
            else
            {
                $totalAmount = 0 ;
            }
            ?>
            <!-- <p class="box-p">RM150</p> -->
            <p class="box-p">RM<?php echo $totalAmount;?></p>
        </div>   
        </a> 
    	<div class="three-div-width dashboard-box second-third-box">
        	<img src="img/icon6.png" alt="<?php echo _USERDASHBOARD_AVAILABLE_BALANCE ?>" title="<?php echo _USERDASHBOARD_AVAILABLE_BALANCE ?>">
            <p class="box-p third-first-p"><b><?php echo _USERDASHBOARD_AVAILABLE_BALANCE ?></b></p>
            <!-- <p class="box-p">RM150</p> -->
            <p class="box-p">RM<?php echo $userData->getAmount();?></p>
        </div>    
    </div>    

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
