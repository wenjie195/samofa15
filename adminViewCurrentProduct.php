<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$productDetails = getProduct($conn, "WHERE display ='1' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <!-- <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" /> -->
    <meta property="og:title" content="Product Details | Samofa 莎魔髪" />
    <title>Product Details | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<?php include 'css.php'; ?> 
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 menu-distance75 min-height-with-flower">
	<h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMINHEADER_CURRENT_PRODUCT ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="overflow-scroll-div same-padding">
        
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                    <th><?php echo _PRODUCTDETAILS_IMAGE ?></th>
                    <th><?php echo _PRODUCTDETAILS_ORI_PRICE ?> (RM)</th>
                    <th><?php echo _PRODUCTDETAILS_DESCRIPTION ?></th>
                </tr>
            </thead>
            <tbody>

                <?php
                if($productDetails)
                {
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productDetails[$cnt]->getName();?></td>
                            <td>
                                <div class="table-review-comment">
                                    <img src="ProductImages/<?php echo $productDetails[$cnt]->getImages();?>" class="product-img" >
                                </div>
                            </td>
                            <td><?php echo $productDetails[$cnt]->getPrice();?></td>
                            <td><?php echo $productDetails[$cnt]->getDescription();?></td>
                        </tr>
                    <?php
                    }
                }
                ?> 

            </tbody>
        </table>

    </div>
</div>    

<div class="clear"></div>
<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>