<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$allWithdrawal = getWithdrawal($conn, " WHERE status != 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="Admin Withdrawal Report | Samofa 莎魔髪" />
    <title>Admin Withdrawal Report | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?php include 'css.php'; ?>
    
</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_WITHDRAWAL_REPORT ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    
    <div class="width100 same-padding container-div1">
   
        <div class="overflow-scroll-div">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <th><?php echo _ADMIN_AMOUNT ?></th>
                        <th><?php echo _USERDASHBOARD_STATUS ?></th>
                        <th><?php echo _ADMIN_REQUESTED_DATE ?></th>
                        <th><?php echo _ADMIN_DETAILS ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($allWithdrawal)
                    {
                        for($cnt = 0;$cnt < count($allWithdrawal) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allWithdrawal[$cnt]->getBankAccHolder();?></td>
                                <td><?php echo $allWithdrawal[$cnt]->getAmount();?></td>

                                <td><?php echo $allWithdrawal[$cnt]->getStatus();?></td>
                                <td><?php echo $allWithdrawal[$cnt]->getDateCreated();?></td>

                                <td>
                                    <!-- <form method="POST" action="userWithdrawlDetails.php" class="hover1"> -->
                                    <form method="POST" action="adminViewWithdrawalDetails.php" class="hover1">
                                        <button class="clean hover1 transparent-button pointer" type="submit" name="withdrawal_uid" value="<?php echo $allWithdrawal[$cnt]->getWithdrawalUid();?>">
                                            <img src="img/details1.png" class="edit-icon1 hover1a" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            <img src="img/details2.png" class="edit-icon1 hover1b" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                        </button>
                                    </form>                  
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?> 
                </tbody>
            </table>
        </div>

    </div>    

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
