<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
//apply in all
define("_MAINJS_ALL_LOGOUT", "Logout");
//index
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
//JS
define("_JS_FOOTER", "©2020 Samofa, All Rights Reserved.");
define("_JS_LOGIN", "Login");
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_FULLNAME", "Fullname");
define("_JS_NEW_PASSWORD", "New Password");
define("_JS_CURRENT_PASSWORD", "Current Password");
define("_JS_RETYPE_PASSWORD", "Retype Password");
define("_JS_RETYPE_NEW_PASSWORD", "Retype New Password");
define("_JS_VIEW_PASSWORD", "View Password");
define("_JS_RETYPE_REFERRER_NAME", "Referrer Name");
define("_JS_REMEMBER_ME", "Remember Me");
define("_JS_FORGOT_PASSWORD", "Forgot Password?");
define("_JS_FORGOT_TITLE", "Forgot Password");
define("_JS_EMAIL", "Email");
define("_JS_SIGNUP", "Sign Up");
define("_JS_FIRSTNAME", "First Name");
define("_JS_LASTNAME", "Last Name");
define("_JS_GENDER", "Gender");
define("_JS_MALE", "Male");
define("_JS_FEMALE", "Female");
define("_JS_BIRTHDAY", "Birthday");
define("_JS_COUNTRY", "Country");
define("_JS_MALAYSIA", "Malaysia");
define("_JS_SINGAPORE", "Singpore");
define("_JS_PHONE", "Phone No.");
define("_JS_REQUEST_TAC", "Request TAC");
define("_JS_TYPE", "Type");
define("_JS_SUBMIT", "Submit");
define("_JS_PLACEORDER", "Place Order");
define("_JS_WITHDRAW_AMOUNT", "Withdraw Amount");
define("_JS_SUCCESS", "Success");
define("_JS_CLOSE", "Close");
define("_JS_ERROR", "Error");
define("_JS_SPONSOR_ID", "Sponsor ID");
define("_JS_ENROLLMENT_PRODUCT", "Enrollment Product");
define("_JS_FLOWER", "Flower");
define("_JS_SELECT_PRODUCT", "Choose a Product");
//HEADERBEFORELOGIN
define("_HEADERBEFORELOGIN_LOGIN", "Login");
define("_HEADERBEFORELOGIN_REGISTER", "Register");
define("_HEADERBEFORELOGIN_HOME", "Home");
define("_HEADERBEFORELOGIN_PRODUCT", "Product");
define("_HEADERBEFORELOGIN_ANNOUNCEMENT", "Annoucement");
define("_HEADERBEFORELOGIN_MY_TEAM", "My Team");
define("_HEADERBEFORELOGIN_MY_WALLET", "My Wallet");
define("_HEADERBEFORELOGIN_PROFILE", "Profile");
define("_HEADERBEFORELOGIN_EDIT_PROFILE", "Edit Profile");
define("_HEADERBEFORELOGIN_EDIT_PASSWORD", "Edit Password");
define("_HEADERBEFORELOGIN_EDIT_EPIN", "E-Pin");
define("_HEADERBEFORELOGIN_REPORT", "Report");
define("_HEADERBEFORELOGIN_COMMISSION_REPORT", "Commission Report");
define("_HEADERBEFORELOGIN_WITHDRAWAL_REPORT", "Withdrawal Report");
define("_HEADERBEFORELOGIN_PURCHASE_REPORT", "Purchase Report");
define("_HEADERBEFORELOGIN_TEAM_MEMBER", "Team Member");
define("_HEADERBEFORELOGIN_MY_PROFILE", "My Profile");
//HEADER
define("_HEADER_ORDER", "Product");
//INDEX
define("_INDEX_QUOTE1", "Only by turning the mood of complaining about the environment into the power of progress, success can be guaranteed.");
define("_INDEX_QUOTE_AUTHOR1", "Romain Rolland");
define("_INDEX_QUOTE2", "First comes health, second personal beauty, then wealth honestly come by.");
define("_INDEX_QUOTE_AUTHOR2", "Plato");
define("_INDEX_QUOTE3", "As long as there is a belief and pursuit, women can endure any hardship and adapt to the environment.");
define("_INDEX_QUOTE_AUTHOR3", "Ding Ling");
define("_INDEX_SAMOFA", "Samofa");
define("_INDEX_CONTACT_NO", "Contact No");
define("_INDEX_EMAIL", "Email");
define("_INDEX_INTERESTED_IN_US", "Interested in us?");
define("_INDEX_OUR_PRODUCT", "Our Product");
define("_INDEX_MAGIC_DESC", "Your Fat Removal Magician");
define("_INDEX_MAGIC_DESC1", "Slimming + is a powder mix made by a combination of white kidney bean extract phase II, psyllium husk, inulin, multi-enzyme, konjac and aloe vera that work effectively in slimming and prevents yo-yo effects.");
define("_INDEX_MAGIC_DESC2", "Have daily nutrients and helps fat loss naturally if taken with a balanced diet and regular exercise.");
define("_INDEX_HAIR_SERUM_SHORT", "Hair Shampoo & Serum");
define("_INDEX_HAIR_SERUM", "Purifying Balancing Shampoo & Serum Ultimate Elixir");
define("_INDEX_SERUM_DESC", "This is a deep repair wool scale hair care artifact. To restore the natural water balance, nourish and restore damaged hair.
");
define("_INDEX_QUANTITY", "Quantity");
define("_INDEX_ADD_TO_CART", "Add to Cart");
define("_INDEX_FUNCTION", "Functions & Benefits");
define("_INDEX_BLOCK_SUGAR", "Effectively blocks 66% sugar control");
define("_INDEX_BLOCK_CARB", "Effectively blocks 66% carbohydrates");
define("_INDEX_BLOCK_FAT_BURNING", "Fat Burning");
define("_INDEX_BLOCK_DETOX", "Detoxification");
define("_INDEX_BODY_SLIM_DOWN", "Body Slim Down");
define("_INDEX_BODY_FIRM_UP", "Body Firm Up");
define("_INDEX_BODY_SHAPE", "Body Shape");
define("_INDEX_ANTI_HUNGER", "Anti-Hunger");
define("_INDEX_USE_METHOD", "Use Method");
define("_INDEX_STEP1", "150ml～200ml room temperature water, mixed with 1sachet of MAGICBLOCA, shake well and drink.");
define("_INDEX_STEP2", "Open the sachet of MAGICBLOCA directly and pour it into the mouth slowly.");
define("_INDEX_INGREDIENTS", "Click to View Ingredients");
define("_INDEX_MAIN_INGREDIENTS", "Main Ingredients");
define("_INDEX_MIX_FRUIT_POWDER", "Mix Fruit Powder");
define("_INDEX_MIX_FRUIT_POWDER_DESC", "Mixed with pomegranate, lemon and pineapple juice, they are rich in nutrients and consist of various vitamins. Besides, they have good health effects on the cardiovascular system and intestinal system of the human body, and also contain powerful anti-cancer effects.");
define("_INDEX_PSYLLIUM_HUSK", "Psyllium Husk");
define("_INDEX_PSYLLIUM_HUSK_DESC", "It can enhance the amount of dietary fiber and play an important role in regulating the health of the gastrointestinal tract. For those with a high incidence of diabetes, a high-fiber diet can prevent the onset of diabetes and reduce weight.");
define("_INDEX_INULIN", "Inulin");
define("_INDEX_INULIN_DESC", "Is a prebiotic that promotes the growth of beneficial bacteria in the intestine. Moderate intake of inulin can help improve hyperlipidemia, relieve constipation, and help lose weight.");
define("_INDEX_WHITE_KIDNEY_BEAN", "White Kidney Bean (Phase 2)");
define("_INDEX_WHITE_KIDNEY_BEAN_DESC", "It delays the digestion and absorption of complex carbohydrates, reduces starch calories absorption by 66%, reduces postprandial blood glucose response and assists in weight management and body shaping.");
define("_INDEX_POLY", "Polysaccharides and Dietary Fiber");
define("_INDEX_INSOLUBLE_DIETARY_FIBER", "Insoluble Dietary Fiber");
define("_INDEX_INSOLUBLE_DIETARY_FIBER_DESC", "Absorb water, soften stools, increase stool volume, stimulate bowel movements, and accelerate bowel movements to reduce the time that harmful substances in the stool contact the bowel and reduce the probability of bowel cancer.");
define("_INDEX_WATER_SOLUBLE_DIETARY_FIBER", "Water - Soluble Dietary Fiber");
define("_INDEX_WATER_SOLUBLE_DIETARY_FIBER_DESC", "Adjusting the metabolism of sugars and lipids. It has a good effect on reducing the cholesterol content of the body and preventing cardiovascular diseases.");
define("_INDEX_FLAVONE", "Flavone");
define("_INDEX_FLAVONE_DESC", "Bioflavonoids have a variety of biological living things, and have important functions such as antibacterial, anti-inflammatory, anti-mutation, antihypertensive, clearing heat and detoxifying, improving microcirculation, anti-tumor and anti-oxidation.");
define("_INDEX_AMYLASE", "α - Amylase Inhibitor, α - AI");
define("_INDEX_AMYLASE_DESC", "A glycoside hydrolase inhibitor. It inhibits the activity of saliva and pancreatic α-amylase in the intestine, hinders the digestion and absorption of starch and other carbohydrates in 60% of food, selectively reduces sugar intake, lowers blood sugar content, and reduces fat synthesis, thereby reducing glucose, weight loss and prevention of obesity.");
define("_INDEX_AMYLASE_DESC2", "The α-AI extracted from white beans has high activity and has a strong inhibitory effect on mammalian pancreatic α-amylase. It has been used as a slimming health food abroad.");
define("_INDEX_MULTIENZYME", "MultiEnzyme");
define("_INDEX_MULTIENZYME_DESC", "Help the body to catabolize protein, starch and fat in the body, improve immunity, improve gastrointestinal digestion, promote metabolism, accelerate the elimination of metabolic waste and toxic substances in the body, delay aging, and prevent disease.");
define("_INDEX_KONJAC", "Konjac");
define("_INDEX_KONJAC_DESC", "It is for beauty, anti - aging, lowering blood pressure, etc, therefore it’s not only delicious but also very practical. Because it is low in fat, low in sugar, and low in calories, it is wonderful for treating diabetes and high blood pressure. In addition, the human digestive system does not have the ability to digest and absorb it, so it can help the peristalsis of the stomach and intestines.");
define("_INDEX_ALOE_VERA", "Aloe Vera");
define("_INDEX_ALOE_VERA_DESC", "Aloe vera contains active ingredients such as aloe-emodin, which play a role in promoting appetite and slowing down the intestine. The anthraquinone derivatives contained in aloe release aloe-emodin in the intestine, which can effectively stimulate intestinal peristalsis, thereby effectively improving the symptoms of poor gastrointestinal function and constipation, effectively prevent secondary accumulation of fat.");
define("_INDEX_ALOE_VERA_DESC2", "In addition, the active factors contained in aloe can promote fat burning and inhibit the intestinal absorption of fat in food, thereby playing a role in weight loss. Besides, eating aloe vera can also promote pepsin secretion and help the stomach digest food.");
define("_INDEX_EXTRA_INFO", "Extra Information:");
define("_INDEX_EXTRA1", "Natural ingredients without colouring and preservative.");
define("_INDEX_EXTRA2", "No - laxative.");
define("_INDEX_EXTRA3", "No yo-yo effects.");
define("_INDEX_LEARN_MORE", "Learn More");
define("_INDEX_SUCCESS", "<img src='img/quote1.png' class='quote-img' alt='Success' title='Success'>");
define("_INDEX_HEALTH", "<img src='img/quote2.png' class='quote-img' alt='Health' title='Health'>");
define("_INDEX_FAITH", "<img src='img/quote3.png' class='quote-img' alt='Faith' title='Faith'>");
define("_INDEX_CORE_VALUE", "Core Value");
define("_INDEX_PURSUE_EXCELLENCE", "Pursue Excellence");
define("_INDEX_CONFIDENT", "Confident");
define("_INDEX_TRUST", "Trust");
define("_INDEX_CHARMING", "Charming");
define("_INDEX_REBORN", "Reborn");
define("_INDEX_INTIMATE", "Intimate");
define("_INDEX_TRANSFORM", "Transform");
define("_INDEX_SECURE", "Secure");
define("_INDEX_COMPANY_PURPOSE", "Company Purpose");
define("_INDEX_BRINGING_WEALTH", "Bringing Wealth");
define("_INDEX_BETTER_COMMUNITY", "Build a Better Community");
define("_INDEX_TALENT", "Cultivate Talent");
define("_INDEX_SERVICES", "Our Services Provided");
define("_INDEX_VALUABLE_LIFE", "A Valuable Life");
define("_INDEX_CONSIDERABLE", "Considerable Income");
define("_INDEX_TEAM_TRAINING", "Team Training");
define("_INDEX_LEADERSHIP", "Leadership");
define("_INDEX_HEALTHY_BODY", "Healthy Body");
define("_INDEX_BUILD_CONFIDENCE", "Build Confidence");
define("_INDEX_IMPROVE_QUALITY", "Improve Quality of Life");
define("_INDEX_MORE_FREEDOM", "More Freedom");
define("_INDEX_STEPS", "Steps");
define("_INDEX_LEARN_MORE2", "More Details");
define("_INDEX_ABOUT_US2", "About Us");
define("_INDEX_DOWNLOAD", "Download");
define("_INDEX_PURI", "Purifying Balancing Shampoo");
define("_INDEX_SERUM2", "Serum Ultimate Elixir");
//Userdashboard
define("_USERDASHBOARD_DASHBOARD", "Dashboard");
define("_USERDASHBOARD_DOWNLINE", "Downline");
define("_USERDASHBOARD_FEMALE", "Lady");
define("_USERDASHBOARD_VIEW", "View");
define("_USERDASHBOARD_GEN", "Generation");
define("_USERDASHBOARD_NAME", "Name");
define("_USERDASHBOARD_SPONSOR", "Sponsor");
define("_USERDASHBOARD_LAST_ORDER", "Last Order");
define("_USERDASHBOARD_RANK", "Ranking");
define("_USERDASHBOARD_STATUS", "Status");
define("_USERDASHBOARD_SALES_VALUE", "Sales Value");
define("_USERDASHBOARD_HIERARCHY", "Hierarchy");
define("_USERDASHBOARD_PERSONAL_MILESTONES", "Achievement Milestone");
define("_USERDASHBOARD_START", "Start");
define("_USERDASHBOARD_FREE_TRIP", "Free Personal Trip");
define("_USERDASHBOARD_FREE_GROUP_TRIP", "Free Group Trip");
define("_USERDASHBOARD_FREE_SCHOLARSHIP", "Free Scholarship");
define("_USERDASHBOARD_FREE_CERT_SCHOLARSHIP", "Free Certification Scholarship");
define("_USERDASHBOARD_UNLOCKING_NEW_CAR", "Unlocking New Car Benefits");
define("_USERDASHBOARD_UNLOCKING_SPECIAL_HOUSE_UNIT", "Unlocking Special House Unit");
define("_USERDASHBOARD_PERFORMANCE", "My Performance");
define("_USERDASHBOARD_PACKAGES", "Package(s)");
define("_USERDASHBOARD_GROUP_PERFORMANCE", "Group Performance");
define("_USERDASHBOARD_MY_GROUP", "My Group");
define("_USERDASHBOARD_TOTAL_COMMISSION", "Total Commission");
define("_USERDASHBOARD_TOTAL_WITHDRAWAL", "Total Withdrawal");
define("_USERDASHBOARD_AVAILABLE_BALANCE", "Available Balance");
//Product Details
define("_PRODUCTDETAILS", "Product Details");
define("_PRODUCTDETAILS_NO", "No.");
define("_PRODUCTDETAILS_NAME", "Name");
define("_PRODUCTDETAILS_IMAGE", "Image");
define("_PRODUCTDETAILS_PRICE", "Price");
define("_PRODUCTDETAILS_ORI_PRICE", "Original Price");
define("_PRODUCTDETAILS_DIS_PRICE", "Ranking Member Price");
define("_PRODUCTDETAILS_STOCK", "Stock");
define("_PRODUCTDETAILS_STATUS", "Status");
define("_PRODUCTDETAILS_DESCRIPTION", "Description");
define("_PRODUCTDETAILS_HOT_TOP_SALES", "Hot Sales/Top Sales");
define("_PRODUCTDETAILS_RANK_PRICE", "Ranking Price");
//Magic BloCA
define("_INDEX_MAGIC_SACHET", "(6g per sachet, 20 sachets per box)");
//Hair Serum
define("_HAIR_SERUM_STEP1", "Replenish Essential Nutrients");
define("_HAIR_SERUM_STEP1_DESC", "Replenishes minerals and trace elements needed for hair and also protein to hair.");
define("_HAIR_SERUM_STEP2", "Repair Damaged Hair");
define("_HAIR_SERUM_STEP2_DESC", "Repairs the hair core, smoothes the hair scales, strengthens the hair, and locks the nutrients in hair.");
define("_HAIR_SERUM_STEP3", "Restores Softness and Health");
define("_HAIR_SERUM_STEP3_DESC", "Gradually alleviate hair frizz interference,  restore hair elasticity and gloss.");
define("_HAIR_SERUM_SMOOTH", "Smooth");
define("_HAIR_SERUM_SOFT_AND_SHINY", "Soft and Shiny");
define("_HAIR_SERUM_REPAIR_DAMAGE", "Repair Damage");
define("_HAIR_SERUM_NOURISH", "Nourish");
define("_HAIR_SERUM_NUTRITION", "Nutrition");
define("_HAIR_SERUM_DYNAMIC_FULLNESS", "Dynamic Fullness");
define("_HAIR_SERUM_REFRESH", "Refresh");
define("_HAIR_SERUM_LOVE_CREATION", "Love Creation");
define("_HAIR_SERUM_LOVE_CREATION_DESC", "This product is safe for  pregnant women");
define("_HAIR_SERUM_ALMOND_OIL", "Almond Oil - Improves Hair Texture and Increase Hydration");
define("_HAIR_SERUM_ALMOND_DESC1", "Almond oil contains various nutrients that are good for hair health, such as Omega-3 Fatty Acids, Phospholipids, Vitamin E and Magnesium. It is also effective for treating hair loss and repairing damaged hair.");
define("_HAIR_SERUM_ALMOND_DESC2", "The high level of Vitamin E in almond oil is especially helpful for conditioning hair. Sweet almond oil can be safely used in aromatherapy without side effects. This helps to condition the hair and skin. It provides nourishing and smoothing benefits, making hair thicker, longer, stronger and more shiny.");
define("_HAIR_SERUM_TEA_TREE_OIL", "Tea Tree Oil - Prevent Hair Breakage and Deep Nourish");
define("_HAIR_SERUM_TEA_TREE_OIL_DESC1", "Tea tree oil is extracted from the leaves of the Australian paperbark tree. This tea tree oil extracted from the leaves by steam distillation and has been used by Aboriginal Australians for centuries. Due to its medicinal value, this tree is now planted around the world.");
define("_HAIR_SERUM_TEA_TREE_OIL_DESC2", "For damaged and dry hair, tea tree oil is the best method to protect the hair. It not only makes the hair supple but also strengthens the hair.");
define("_HAIR_SERUM_MONACO_NUT_OIL", "Monaco Nut Oil - Relieves Dry Hair and Restores Healthy Hair");
define("_HAIR_SERUM_MONACO_NUT_OIL_DESC1", "Monaco nut oil can repair dry or damaged hair and restore them to health and shine.");
define("_HAIR_SERUM_MONACO_NUT_OIL_DESC2", "Monaco nuts contain 36.0% protein, 58.8% fat, 72.6% carbohydrates, vitamins B, E trace elements phosphorus, calcium, zinc, and dietary fiber.");
define("_HAIR_SERUM_MONACO_NUT_OIL_DESC3", "It also contains mono- and polyunsaturated fatty acids, including essential fatty acids such as linolenic acid and linoleic acid.");
//View Cart
define("_VIEW_CART_CART", "Cart");
define("_VIEW_CART_PRODUCT", "Product");
define("_VIEW_CART_QUANTITY", "Quantity");
define("_VIEW_CART_TOTAL", "Total");
define("_VIEW_CART_CHECKOUT", "Checkout");
define("_VIEW_CART_BACK", "Go Back");
//Checkout
define("_CHECKOUT_CONTACT", "Contact Information");
define("_CHECKOUT_SHIPPING_ADDRESS", "Shipping Address");
define("_CHECKOUT_ADDRESS1", "Address Line 1");
define("_CHECKOUT_ADDRESS2", "Address Line 2");
define("_CHECKOUT_CITY", "City");
define("_CHECKOUT_ZIP_CODE", "Postcode");
define("_CHECKOUT_STATE", "State");
define("_CHECKOUT_CHECKOUT", "Checkout");
define("_CHECKOUT_NEXT", "Next");
//Shipping
define("_SHIPPING_PAYMENT", "Payment");
define("_SHIPPING_SHIP_TO", "Ship To");
define("_SHIPPING_ALL_TRANS", "All transactions are secure and encrypted.");
define("_SHIPPING_BANK", "Bank");
define("_SHIPPING_ACC_NO", "Account No.");
define("_SHIPPING_BANK_HOLDER", "Bank Account Holder");
define("_SHIPPING_PAYMENT_METHOD", "Payment Method");
define("_SHIPPING_SELECT_PAYMENT_METHOD", "Select a Payment Method");
define("_SHIPPING_AMOUNT", "Amount");
define("_SHIPPING_BANK_REFERENCE", "Bank Transaction Reference");
define("_SHIPPING_BANK_IN_DATE", "Bank In Date");
define("_SHIPPING_BANK_IN_TIME", "Bank In Time");
define("_SHIPPING_UPLOAD_RECEIPT", "Upload Receipt");
//User Downline
define("_USERDOWNLINE", "User Downline");
//Purchase History
define("_PURCHASE_ORDER_HISTORY", "Order History");
define("_PURCHASE_DATE", "Date");
define("_PURCHASE_DETAILS", "Details");
define("_PURCHASE_PENDING", "Pending");
define("_PURCHASE_VIEW", "View");
//Edit Profile
define("_EDITPROFILE_BANK_NAME", "Bank Name");
define("_EDITPROFILE_BANK_ACC_HOLDER", "Bank Account Holder");
define("_EDITPROFILE_BANK_ACC_NO", "Bank Account Number");
define("_EDITPROFILE", "Edit Profile");
//Edit EPIN
define("_EPIN_EDIT_EPIN", "Edit E-Pin");
define("_EPIN_CURRENT_EPIN", "Current E-Pin");
define("_EPIN_NEW_EPIN", "New E-Pin");
define("_EPIN_RETYPE_NEW_EPIN", "Retype New E-Pin");
define("_EPIN_VIEW", "View E-Pin");
define("_EPIN_REGISTER_EPIN", "Register E-Pin");
define("_EPIN", "E-Pin");
define("_EPIN_RETYPE", "Retype E-Pin");
//My Team
define("_MYTEAM", "My Team");
//Member
define("_MEMBER_WITHDRAWAL", "Withdrawal");
define("_MEMBER_WITHDRAWAL_AMOUNT", "Withdrawal Amount");
define("_MEMBER_BANK_DETAILS_MSG", "Click Here to Update Your Bank Account Details Before Making Withdrawal");
define("_MEMBER_ACC_DETAILS", "Personal Info");
define("_MEMBER_BANK_DETAILS", "Bank Details");
define("_MEMBER_DELIVERY_ADDRESS", "Delivery Address");
define("_MEMBER_RECEIVER_EMAIL", "Recipient's Email");
define("_MEMBER_RECEIVER_PHONE_NO", "Recipient's Contact No.");
//ADMIN
define("_ADMIN_DASHBOARD", "Admin Dashboard");
define("_ADMIN_CURRENT_ORDER", "Current Orders");
define("_ADMIN_WITHDRAWAL_REQUEST", "Withdrawal Request");
define("_ADMIN_NEW_REGISTRATION", "Registration Approval");
define("_ADMIN_TOTAL_MEMBERS", "Total Members");
define("_ADMIN_TOTAL_PACKAGE", "Total Package");
define("_ADMIN_TOTAL_REVENUE", "Total Revenue");
define("_ADMIN_TOTAL_COMMISSION", "Total Commission");
define("_ADMIN_TOTAL_COMMISSION_WITHDRAWN", "Total Commission Withdrawn");
define("_ADMIN_EDIT_MEMBER_PROFILE", "Edit Member Profile");
define("_ADMIN_CURRENT_CREDIT", "Current Credit");
define("_ADMIN_EDIT_PASSWORD_EPIN", "Edit Password/E-Pin");
define("_ADMIN_SEARCH", "Search");
define("_ADMIN_EDIT_USER_PASSWORD", "Admin Edit User Password");
define("_ADMIN_EDIT_PASSWORD", "Admin Edit Password");
define("_ADMIN_EDIT_USER_EPIN", "Admin Edit User E-Pin");
define("_ADMIN_EDIT_EPIN", "Edit E-Pin");
define("_ADMIN_VIEW_EPIN", "View E-Pin");
//ADMIN
define("_ADMINHEADER_NETWORK", "Network");
define("_ADMINHEADER_ALLMEMBER", "All Members");
define("_ADMIN_REGISTER_NEW_MEMBER", "Add New Member");
define("_ADMINHEADER_PRODUCTS", "Products");
define("_ADMINHEADER_ADD_NEW_PACKAGE", "Add New Package");
define("_ADMINHEADER_ADD_NEW_PRODUCT", "Add New Product");
define("_ADMINHEADER_CURRENT_PRODUCT", "Current Product/Package");
define("_ADMINHEADER_PROFILE_MANAGEMENT", "Profile");
define("_ADMINHEADER_MEMBER_PROFILE", "Member Profile");
define("_ADMINHEADER_SALES_REPORT", "Sales Report");
define("_ADMINHEADER_PAYOUT_REPORT", "Payout Report");
define("_ADMINHEADER_PACKAGE_REPORT", "Package/Product Sold Report");
define("_ADMINHEADER_CUSTOMER_REPORT", "Sales by Customer Report");
define("_ADMIN_AMOUNT", "Amount");
define("_ADMIN_REQUESTED_DATE", "Requested Date");
define("_ADMIN_DETAILS", "Details");
define("_ADMIN_WITHDRAWAL_DETAILS", "Withdrawal Details");
define("_ADMIN_PAYMENT_METHOD", "Payment Method");
define("_ADMIN_DATE_AND_TIME", "Date and Time");
define("_ADMIN_REASON", "Reason");
define("_ADMIN_ACTION", "Action");
define("_ADMIN_REFERENCE", "Reference");
define("_ADMIN_REASON_OF_REJECT", "Reason of Reject");
define("_ADMIN_APPROVE", "Approve");
define("_ADMIN_REJECT", "Reject");
define("_ADMIN_PERSONAL_SALES", "Personal Sales");
define("_ADMIN_GROUP_SALES", "Group Sales");
define("_ADMIN_LAST_ORDER", "Latest Order");
define("_ADMIN_TITLE", "Title");
define("_ADMIN_CONTENT", "Content");
define("_ADMIN_DATE", "Date");
define("_ADMIN_UPDATE", "Update");
define("_ADMIN_DELETE", "Delete");
define("_ADMIN_ADD_ANNOUNCEMENT", "Add Announcement");
define("_ADMIN_UPDATE_ANNOUNCEMENT", "Update Announcement");
define("_ADMIN_ANNOUNCEMENT", "Announcement");
//MILESTONE
define("_MILESTONE_BONUS1", "Bonus 1");
define("_MILESTONE_BONUS1_DESC", "Direct Sponsor 3 members, group sales reach 6 packages*");
define("_MILESTONE_BONUS1_DESC1", "Direct Sponsor 3 members,");
define("_MILESTONE_BONUS1_DESC2", "group sales reach 6 packages*");
define("_MILESTONE_GROUP_SALES", "of Group Sales");
define("_MILESTONE_BONUS2", "Bonus 2");
define("_MILESTONE_BONUS2_DESC", "Ranking Bonus - Agent*");
define("_MILESTONE_BONUS3", "Bonus 3");
define("_MILESTONE_BONUS3_DESC", "Ranking Bonus - Director*");
define("_MILESTONE_BONUS4", "Bonus 4");
define("_MILESTONE_BONUS4_DESC", "Group sales reach 250 packages*");
define("_MILESTONE_BONUS5", "Bonus 5");
define("_MILESTONE_BONUS5_DESC", "Group sales reach 400 packages*");
define("_MILESTONE_BONUS6", "Bonus 6");
define("_MILESTONE_BONUS6_DESC", "Ranking Bonus - Partner*");
define("_MILESTONE_BONUS7", "Bonus 7");
define("_MILESTONE_BONUS7_DESC", "Group sales reach 850 packages*");
define("_MILESTONE_BONUS8", "Bonus 8");
define("_MILESTONE_BONUS8_DESC", "Annual Bonus - Shareholder 0.5%");
define("_MILESTONE_BONUS8_DESC2", "* Include the repurchase of 1 package");
define("_MILESTONE_SAMOFA_MARKETING", "SAMOFA 2020 System Marketing Plan");
define("_MILESTONE_PRODUCT_PRICE", "Product Price");