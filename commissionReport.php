<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Bonus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$bonusDetails = getBonusDetails($conn, " WHERE referrer_id =? ", array("referrer_id"), array($uid), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="Commission Report | Samofa 莎魔髪" />
    <title>Commission Report | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?php include 'css.php'; ?>

</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _HEADERBEFORELOGIN_COMMISSION_REPORT ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="width100 container-div1">

        <div class="overflow-scroll-div same-padding">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PRODUCTDETAILS_NO ?></th>
                        <th><?php echo _JS_USERNAME ?></th>
                        <th><?php echo _ADMIN_AMOUNT ?></th>
                        <th><?php echo _USERDASHBOARD_STATUS ?></th>
                        <th><?php echo _PURCHASE_DATE ?></th>
                        <th><?php echo _ADMIN_DETAILS ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($bonusDetails)
                    {
                        for($cnt = 0;$cnt < count($bonusDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bonusDetails[$cnt]->getReferrerName();?></td>
                                <td><?php echo $bonusDetails[$cnt]->getAmount();?></td>

                                <td><?php echo $bonusDetails[$cnt]->getStatus();?></td>
                                <td><?php echo date('d/m/Y',strtotime($bonusDetails[$cnt]->getDateCreated()));?></td>

                                <td>
                                    <form method="POST" action="userWithdrawalDetails.php" class="hover1">
                                        <button class="clean hover1 transparent-button pointer" type="submit" name="withdrawal_uid" value="<?php echo $bonusDetails[$cnt]->getReferrerId();?>">
                                            <img src="img/details1.png" class="edit-icon1 hover1a" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                            <img src="img/details2.png" class="edit-icon1 hover1b" alt="<?php echo _ADMIN_DETAILS ?>" title="<?php echo _ADMIN_DETAILS ?>">
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>

    </div>

    <div class="clear"></div>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
