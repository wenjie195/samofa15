<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="Withdrawal Request | Samofa 莎魔髪" />
    <title>Withdrawal Request | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_WITHDRAWAL_REQUEST ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 same-padding">

    <form method="POST" action="utilities/adminVerifyWithdrawalFunction.php" enctype="multipart/form-data">

        <?php
            if(isset($_POST['withdrawal_uid']))
            {
                $conn = connDB();
                $withdrawalDetails = getWithdrawal($conn,"WHERE withdrawal_uid = ? ", array("withdrawal_uid") ,array($_POST['withdrawal_uid']),"s");
            ?>

                <div class="dual-input">
                    <p class="input-top-text"><?php echo _EDITPROFILE_BANK_NAME ?></p>
                    <input class="clean de-input" type="text" value="<?php echo $withdrawalDetails[0]->getBankName();?>" id="update_username" name="update_username" readonly>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text"><?php echo _EDITPROFILE_BANK_ACC_HOLDER ?></p>
                    <input class="clean de-input" type="text" value="<?php echo $withdrawalDetails[0]->getBankAccHolder();?>" id="update_firstname" name="update_firstname" readonly>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-text"><?php echo _EDITPROFILE_BANK_ACC_NO ?></p>
                    <input class="clean de-input" type="text" value="<?php echo $withdrawalDetails[0]->getBankAccNo();?>" id="update_lastname" name="update_lastname" readonly>
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text"><?php echo _ADMIN_AMOUNT ?></p>
                    <input class="clean de-input" type="text" value="<?php echo $withdrawalDetails[0]->getAmount();?>" id="update_firstname" name="update_firstname" readonly>
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-text"><?php echo _ADMIN_PAYMENT_METHOD ?></p>
                    <input class="clean de-input" type="text" placeholder="<?php echo _ADMIN_PAYMENT_METHOD ?>" id="method" name="method">
                </div>

                

                <div class="dual-input second-dual-input">
                    <p class="input-top-text"><?php echo _ADMIN_REFERENCE ?></p>
                    <input class="clean de-input" type="text" placeholder="<?php echo _ADMIN_REFERENCE ?>" id="reference" name="reference">
                </div>

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-text"><?php echo _ADMIN_REASON_OF_REJECT ?></p>
                    <input class="clean de-input" type="text" placeholder="<?php echo _ADMIN_REASON_OF_REJECT ?>" id="rejected_reason" name="rejected_reason">
                </div>

            

                <input class="clean de-input" type="hidden" value="<?php echo $withdrawalDetails[0]->getWithdrawalUid();?>" id="withdrawal_uid" name="withdrawal_uid" readonly>

				<div class="clear"></div>
                <div class="width100 text-center top-bottom-distance">
                    <button class="clean button-width transparent-button dark-pink-button reject-btn" type="submit" id="withdrawal_status" name="withdrawal_status" value="REJECTED"><?php echo _ADMIN_REJECT ?></button>                   
                    <button class="clean button-width transparent-button dark-pink-button approve-btn" type="submit" id="withdrawal_status" name="withdrawal_status" value="APPROVED"><?php echo _ADMIN_APPROVE ?></button>
                 
                </div>


            <?php
        }
        ?>
    </form>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<!-- <script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            case 'ACCEPTED':
                form=document.getElementById('paymentVerifiedForm');
                form.action='utilities/adminApprovedWithdrawal.php';
                form.submit();
            break;
            case 'REJECT':
                form=document.getElementById('paymentVerifiedForm');
                form.action='utilities/adminRejecteddWithdrawal.php';
                form.submit();
            break;
        }

    }
</script> -->

</body>
</html>