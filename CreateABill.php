<?php
/*
 * This file shows how to Create A Bill using Billplz class
 * The commented line is Optional parameter
 * 
 */

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

$amount = $_GET['amount'];
// $name = $_GET['name'];

require_once dirname(__FILE__) . '/classes/BillPlz.php';

//$api_key = 'f3cf5434-8b1e-48f7-8612-a879816f15b5';
$api_key ='095cecd6-7475-4aa1-b0c7-a08b8eda0b79';
$a = new Billplz;
$a->setName("Reva");
$a->setAmount($amount);
$a->setEmail('reva@gmail.com');
$a->setDescription("Kedai kasut ");
$a->setPassbackURL('http://callback-url.com', 'http://redirect-url.com');
//$a->setCollection('collect_id');
//$a->setPassbackURL('http://callback-url.com');
$a->setReference_1_Label('Item To Buy');
$a->setReference_1('Item Name');

$a->setMobile('999');
//$a->setDeliver('0'); //No Notification
//$a->setDeliver('1'); //Email Notification
//$a->setDeliver('2'); //SMS Notification
//$a->setDeliver('3'); //Email & SMS Notification
//$a->create_bill($api_key, true);
$a->create_bill($api_key);
//echo $a->getURL();
header('Location: ' .$a->getURL());