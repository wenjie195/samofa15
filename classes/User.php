<?php
class User {
    /* Member variables */
    var $id,$uid,$memberId,$username,$email,$firstname,$lastname,$icno,$password,$salt,$birthDate,$country,$phoneNo,$address,$addressTwo,$zipcode,$state,
            $rank,$userRank,$epin,$epinSalt,$bankName,$bankAccHolder,$bankAccNo,$amount,$status,$downlineNo,$salesValue,$bonus,$sales,$loginType,
                $userType,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $uid
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getMemberID()
    {
        return $this->memberId;
    }

    /**
     * @param mixed $memberId
     */
    public function setMemberID($memberId)
    {
        $this->memberId = $memberId;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getIcno()
    {
        return $this->icno;
    }

    /**
     * @param mixed $icno
     */
    public function setIcno($icno)
    {
        $this->icno = $icno;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getAddressB()
    {
        return $this->addressTwo;
    }

    /**
     * @param mixed $addressTwo
     */
    public function setAddressB($addressTwo)
    {
        $this->addressTwo = $addressTwo;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param mixed $rank
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return mixed
     */
    public function getUserRank()
    {
        return $this->userRank;
    }

    /**
     * @param mixed $rankAValue
     */
    public function setUserRank($userRank)
    {
        $this->userRank = $userRank;
    }

    /**
     * @return mixed
     */
    public function getDownlineNo()
    {
        return $this->downline_no;
    }

    /**
     * @param mixed $rankAValue
     */
    public function setDownlineNo($downlineNo)
    {
        $this->downline_no = $downlineNo;
    }

    /**
     * @return mixed
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param mixed $rankAValue
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * @return mixed
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param mixed $rankAValue
     */
    public function setSales($sales)
    {
        $this->sales = $sales;
    }

    /**
     * @return mixed
     */
    public function getEpin()
    {
        return $this->epin;
    }

    /**
     * @param mixed $epin
     */
    public function setEpin($epin)
    {
        $this->epin = $epin;
    }

    /**
     * @return mixed
     */
    public function getEpinSalt()
    {
        return $this->epinSalt;
    }

    /**
     * @param mixed $epinSalt
     */
    public function setEpinSalt($epinSalt)
    {
        $this->epinSalt = $epinSalt;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankAccHolder()
    {
        return $this->bankAccHolder;
    }

    /**
     * @param mixed $bankAccHolder
     */
    public function setBankAccHolder($bankAccHolder)
    {
        $this->bankAccHolder = $bankAccHolder;
    }

    /**
     * @return mixed
     */
    public function getBankAccNo()
    {
        return $this->bankAccNo;
    }

    /**
     * @param mixed $bankAccNo
     */
    public function setBankAccNo($bankAccNo)
    {
        $this->bankAccNo = $bankAccNo;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSalesValue()
    {
        return $this->salesValue;
    }

    /**
     * @param mixed $salesValue
     */
    public function setSalesValue($salesValue)
    {
        $this->salesValue = $salesValue;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","member_id","username","email","firstname","lastname","icno","password","salt","birth_date","country","phone_no","address","address_two",
                        "zipcode","state","rank","user_rank","epin","salt_epin","bank_name","bank_account_holder",
                            "bank_account_no","amount","status","downline_no","sales_value","bonus","sales","login_type","user_type","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$memberId,$username,$email,$firstname,$lastname,$icno,$password,$salt,$birthDate,$country,$phoneNo,$address,$addressTwo,$zipcode,$state,
                            $rank,$userRank,$epin,$epinSalt,$bankName,$bankAccHolder,$bankAccNo,$amount,$status,$downlineNo,$salesValue,
                                $bonus,$sales,$loginType,$userType,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setId($id);
            $user->setUid($uid);
            $user->setMemberID($memberId);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setFirstname($firstname);
            $user->setLastname($lastname);
            $user->setIcno($icno);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setBirthDate($birthDate);
            $user->setCountry($country);
            $user->setPhoneNo($phoneNo);
            $user->setAddress($address);
            $user->setAddressB($addressTwo);
            $user->setZipcode($zipcode);
            $user->setState($state);
            $user->setRank($rank);
            $user->setUserRank($userRank);
            $user->setDownlineNo($downlineNo);
            $user->setSales($sales);
            $user->setBonus($bonus);

            $user->setEpin($epin);
            $user->setEpinSalt($epinSalt);
            $user->setBankName($bankName);
            $user->setBankAccHolder($bankAccHolder);
            $user->setBankAccNo($bankAccNo);
            $user->setAmount($amount);

            $user->setStatus($status);
            $user->setSalesValue($salesValue);
            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
