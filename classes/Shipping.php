<?php
class Product{
    /* Member variables */
    var $id,$name,$price,$rankOri,$rankA,$rankB,$rankC,$stock,$buyStock,$totalPruchaseValue,$display,$type,$description,$images,$productType,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getRankOri()
    {
        return $this->rankOri;
    }

    /**
     * @param mixed $rankOri
     */
    public function setRankOri($rankOri)
    {
        $this->rankOri = $rankOri;
    }

    /**
     * @return mixed
     */
    public function getRankA()
    {
        return $this->rankA;
    }

    /**
     * @param mixed $rankA
     */
    public function setRankA($rankA)
    {
        $this->rankA = $rankA;
    }

    /**
     * @return mixed
     */
    public function getRankB()
    {
        return $this->rankB;
    }

    /**
     * @param mixed $rankB
     */
    public function setRankB($rankB)
    {
        $this->rankB = $rankB;
    }

    /**
     * @return mixed
     */
    public function getRankC()
    {
        return $this->rankC;
    }

    /**
     * @param mixed $rankC
     */
    public function setRankC($rankC)
    {
        $this->rankC = $rankC;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $id
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getBuyStock()
    {
        return $this->buyStock;
    }

    /**
     * @param mixed $buyStock
     */
    public function setBuyStock($buyStock)
    {
        $this->buyStock = $buyStock;
    }

    /**
     * @return mixed
     */
    public function getTotalPurchaseValue()
    {
        return $this->totalPruchaseValue;
    }

    /**
     * @param mixed $totalPruchaseValue
     */
    public function setTotalPurchaseValue($totalPruchaseValue)
    {
        $this->totalPruchaseValue = $totalPruchaseValue;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @param mixed $productType
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","name","price","rankOri","rankA","rankB","rankC","stock","buy_stock","total_price","display","type","description","images","product_type",
                                "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$name,$price,$rankOri,$rankA,$rankB,$rankC,$stock,$buyStock,$totalPruchaseValue,$display,$type,$description,$images,$productType,
                                $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product();

            $class->setId($id);
            $class->setName($name);
            $class->setPrice($price);
            $class->setRankOri($rankOri);
            $class->setRankA($rankA);
            $class->setRankB($rankB);
            $class->setRankC($rankC);
            $class->setStock($stock);
            $class->setBuyStock($buyStock);
            $class->setTotalPurchaseValue($totalPruchaseValue);
            $class->setDisplay($display);
            $class->setType($type);
            $class->setDescription($description);
            $class->setImages($images);

            $class->setProductType($productType);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createProductList($products,$rank,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true){
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";

    if(!$products){
        return $productListHtml;
    }
    //$CumPrice = 0;
    $subtotal = 0;
    $shippingfees = 30;
    $total = 0;
    $index = 0;

    foreach ($products as $product){
        $quantity = 0;
        if($postQuantityRows){
            $quantity = $postQuantityRows[$index];
        }

        // if($rank=="A"){
        //     $rankPrice = $product->getRankA();
        // }
        // else if ($rank=="B"){
        //     $rankPrice = $product->getRankB();
        // }
        // else if ($rank=="C"){
        //     $rankPrice = $product->getRankC();
        // }

        if($rank=="Rising Star"){
            $rankPrice = $product->getRankA();
        }
        else if ($rank=="Group Star"){
            $rankPrice = $product->getRankB();
        }
        else if ($rank=="Group Boss"){
            $rankPrice = $product->getRankC();
        }
        else if ($rank=="Member"){
            $rankPrice = $product->getRankOri();
        }
        else
        {
            // $rankPrice = "248";
            $rankPrice = $product->getRankOri();
        }

        $totalPrice = "";


        if($quantity <= 0 && !$isIncludeNotSelectedProductToo){

            $productListHtml .= '<div style="display: none;">';

        }else{
            $totalPrice = $quantity * $rankPrice;
            // $CumPrice += $totalPrice;
            $subtotal += $totalPrice;
            $_SESSION['total'] = $total = $subtotal+$shippingfees;

            $productListHtml .= '<div style="display: block;">';

        }

        $conn=connDB();
        //$productArray = getProduct($conn);
                  $id  = $product->getName();
              // Include the database configuration file


              // Get images from the database
              $query = $conn->query("SELECT images,name FROM product WHERE name = '$id'");

              if($query->num_rows > 0)
        {
                  while($row = $query->fetch_assoc())
                    {
                      $imageURL = './ProductImages/'.$row["images"];

        $productListHtml .= '

              <!-- Product -->

                <table class="table-css">
                    <tbody>
                        <tr>
                            <td>
                                <img src="'.$imageURL.'" class="product-img" alt="'.$product->getName().'" title="'.$product->getName().'">
                            </td>
                            <td>'.$quantity.'</td>
                            <td>'.$product->getName().'</td>
                            <td>RM '.$totalPrice.'.00</td>
                            <input class="clean two-box-input" type="hidden" id="insert_subprice" name="insert_subprice" value=" ' .$totalPrice.' ">
                        </tr>
                    </tbody>
                </table>

            </div>
        ';
                    }
        }
        $index++;

    }

    //$productListHtml .= '<h2 class="product-name-h2">Subtotal : ' .$CumPrice.' Points<h2>';
    $productListHtml .=
    '
    <div class="width100 text-center margin-top40">
		<p class="pink-text">Subtotal: Calculated at next step</p>
		<input class="clean two-box-input" type="hidden"
                id="insert_subtotal" name="insert_subtotal" value=" ' .$subtotal.' ">
	</div>
    <div class="width100 text-center">
		<p class="pink-text">Shipping Fees: RM' .$shippingfees.'.00</p>
	</div>		
    <div class="width100 text-center">
		<p class="pink-text">Total: RM' .$total.'.00</p>
		<input class="clean two-box-input" type="hidden"
                id="insert_total" name="insert_total" value=" ' .$total.' ">		
	</div>		





    ';

    return $productListHtml;
}

function getProductPrice($conn,$productId){
    $price = 0;

    $productRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $price = $productRows[0]->getPrice();
    }

    return $price;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

//        if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
//        }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid,$rank){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];
        $orderId = insertDynamicData($conn,"orders",array("uid"),array($uid),"s");

        if($orderId){
            $totalPrice = 0;

            // if($rank=="A"){
            //     $rankPrice = $product->getRankA();
            // }
            // else if ($rank=="B"){
            //     $rankPrice = $product->getRankB();
            // }
            // else if ($rank=="C"){
            //     $rankPrice = $product->getRankC();
            // }

            if($rank=="Rising Star"){
                $rankPrice = $product->getRankA();
            }
            else if ($rank=="Group Star"){
                $rankPrice = $product->getRankB();
            }
            else if ($rank=="Group Boss"){
                $rankPrice = $product->getRankC();
            }
            else if ($rank=="Member"){
                $rankPrice = $product->getRankOri();
            }
            else
            {
                $rankPrice = "248";
            }

            for($index = 0; $index < count($shoppingCart); $index++){
                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                // $_SESSION['productName'] = $productId;
                $quantity = $thisCart['quantity'];
                $originalPrice = getProductPrice($conn,$productId,$rank);
                $totalPrice += ($originalPrice * $rankPrice);

                if(!insertDynamicData($conn,"product_orders",array("product_id","order_id","quantity","final_price","original_price","discount_given"),
                    array($productId,$orderId,$quantity,$originalPrice,$originalPrice,0),"iiiddd")){
                    promptError("error creating order for product : $productId");
                }
            }


            // todo this 2 code is AFTER payment successfully done then only execute
            // insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
            // initiateReward($conn,$orderId,$uid,$totalPrice);

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$rank,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getProduct($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$rank,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}