<?php
class LeadershipBonusReport {
    /* Member variables */
    var $id,$uid,$newUser,$receiverUsername,$receiverUID,$bonusType,$bonus,$status,$dateCreated;

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setID($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $id
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * @param mixed $id
     */
    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getNewUser()
    {
        return $this->new_user;
    }

    /**
     * @param mixed $id
     */
    public function setNewUser($newUser)
    {
        $this->new_user = $newUser;
    }

    /**
     * @return mixed
     */
    public function getReceiveUid()
    {
        return $this->receive_uid;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveUID($receiverUID)
    {
        $this->receive_uid = $receiverUID;
    }

    /**
     * @return mixed
     */
    public function getReceiveUsername()
    {
        return $this->receive_username;
    }

    /**
     * @param mixed $id
     */
    public function setReceiveUsername($receiverUsername)
    {
        $this->receive_username = $receiverUsername;
    }

    /**
     * @return mixed
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param mixed $id
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * @return mixed
     */
    public function getBonusType()
    {
        return $this->bonus_type;
    }

    /**
     * @param mixed $id
     */
    public function setBonusType($bonusType)
    {
        $this->bonus_type = $bonusType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $id
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;
    }



}

function getOtherBonus($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","new_user","receive_username","receive_uid","bonus_type","amount","status","date_created");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"other_bonus");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$newUser,$receiverUsername,$receiverUID,$bonusType,$bonus,$status,$dateCreated);
                    // array("id","withdrawal_number", "withdrawal_status", "final_amount","date_create");

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new LeadershipBonusReport();
            $class->setID($id);
            $class->setUID($uid);
            $class->setNewUser($newUser);
            $class->setReceiveUsername($receiverUsername);
            $class->setReceiveUID($receiverUID);
            $class->setBonusType($bonusType);
            $class->setBonus($bonus);
            $class->setStatus($status);
            $class->setDateCreated($dateCreated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}

            //todo this 2 code is AFTER payment successfully done then only execute
//            insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
//
