<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dxforextrade88.com/" />-->
    <meta property="og:title" content="Samofa 莎魔髪" />
    <title>Samofa 莎魔髪</title>
    <!--<link rel="canonical" href="https://dxforextrade88.com/" />-->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //include 'headerBeforeLogin.php'; ?> -->
<?php include 'headerAfterLogin.php'; ?>

<div class="video-div width100 menu-distance">
  <div id="fb-root"></div>
  <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>

  
  <div class="fb-video" data-href="https://www.facebook.com/235643117303835/videos/692348604599652/" data-width="1920" data-show-text="false" class="home-vido" autoplay loop>
  </div>
</div>
<!--
<div class="video-div width100 menu-distance">
	<video class="home-video" autoplay poster="img/samofa-poster.jpg" id="videoPlayer" loop>
    	<source src="img/samofa-video.mp4" type="video/mp4">
    </video>
</div>-->
<div class="width100 same-padding quote-div2 quote-bg overflow">
	<div class="three-div-width text-center">
    		<p class="big-quote"><?php echo _INDEX_SUCCESS ?></p>
            <p class="dark-pink-text quote-p2">
                "<?php echo _INDEX_QUOTE1 ?>"
            </p>
            <div class="clear"></div>
            <div class="quote-border"></div>
            <div class="clear"></div>
            <p class="light-pink-text quote-author2">
                <?php echo _INDEX_QUOTE_AUTHOR1 ?>
            </p>    	
    </div>
	<div class="three-div-width mid-three-div-width-margin text-center">
            <p class="big-quote"><?php echo _INDEX_HEALTH ?></p>
            <p class="dark-pink-text quote-p2">
                "<?php echo _INDEX_QUOTE2 ?>"
            </p>
            <div class="clear"></div>
            <div class="quote-border"></div>
            <div class="clear"></div>            
            <p class="light-pink-text quote-author2">
                <?php echo _INDEX_QUOTE_AUTHOR2 ?>
            </p>    	
    </div>
	<div class="three-div-width text-center">
    		<p class="big-quote"><?php echo _INDEX_FAITH ?></p>
            <p class="dark-pink-text quote-p2">
                "<?php echo _INDEX_QUOTE3 ?>"
            </p>
            <div class="clear"></div>
            <div class="quote-border"></div>
            <div class="clear"></div>            
            <p class="light-pink-text quote-author2">
                <?php echo _INDEX_QUOTE_AUTHOR3 ?>
            </p>    	
    </div>        
</div>
<div class="clear"></div>
<div class="width100 same-padding text-center product-big-div">
	<h1 class="dark-pink-text hi-title contact-title big-header-color"><?php echo _INDEX_OUR_PRODUCT ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="float-left product-img-div">
    	<img src="img/magic-bloca.png" class="mid-img" alt="Magic BloCA 脱糖宝" title="Magic BloCA 脱糖宝">
    </div>
    <div class="float-right product-desc-div">
    	<h2 class="dark-pink-text product-title">Magic BloCA<sup class="dark-pink-text smaller-h1">TM</sup> 脱糖宝</h2>
        <h3 class="mild-pink-text h3-sub"><?php echo _INDEX_MAGIC_DESC ?></h3>
        <h3 class="mild-pink-text desc-h3"><?php echo _INDEX_MAGIC_DESC1 ?></h3>
        <div class="clear"></div>
        <div class="width100 text-center">
        	<a href="magicBloca.php"><div class="pink-button button-width margin-auto learn-btn"><b class="white-text"><?php echo _INDEX_LEARN_MORE ?></b></div></a>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
	<div class="float-right product-img-div extra-margin-top">
    	<img src="img/purifying-balancing-shampoo-serum-ultimate-elixir.png" class="mid-img" alt="<?php echo _INDEX_HAIR_SERUM ?>" title="<?php echo _INDEX_HAIR_SERUM ?>">
    </div>
    <div class="float-left product-desc-div product-desc-div2">
    	<h2 class="dark-pink-text product-title"><?php echo _INDEX_HAIR_SERUM ?></h2>
        <h3 class="mild-pink-text desc-h3"><?php echo _INDEX_SERUM_DESC ?></h3>
        <div class="width100 text-center">
            <!-- <a href="hairSerum.php"><div class="pink-button button-width margin-auto learn-btn"><b class="white-text"><?php echo _INDEX_LEARN_MORE ?></b></div></a> -->
            <a href="hairSerum.php"><div class="pink-button button-width margin-auto learn-btn"><b class="white-text"><?php echo _INDEX_LEARN_MORE ?></b></div></a>
        </div>        
    </div>    
    
</div>
<div class="clear"></div>
<div class="width100 overflow big-core-div">
    <div class="top-left-flower-div">
        <img src="img/top-left-flower.png" class="top-left-flower" alt="Samofa 莎魔髪" title="Samofa 莎魔髪">
    </div>
    <div class="width100 overflow same-padding core-div">
		<h1 class="dark-pink-text hi-title contact-title big-header-color text-center"><?php echo _INDEX_CORE_VALUE ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
        <div class="clear"></div>
        <div class="left-core-div">
        	<p class="core-p">
            	<img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_CORE_VALUE ?>">
                <?php echo _INDEX_PURSUE_EXCELLENCE ?>
            </p>
        	<p class="core-p">
            	<img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_CORE_VALUE ?>">
                <?php echo _INDEX_CONFIDENT ?>
            </p>            
        	<p class="core-p">
            	<img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_CORE_VALUE ?>">
                <?php echo _INDEX_TRUST ?>
            </p>
        	<p class="core-p">
            	<img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_CORE_VALUE ?>">
                <?php echo _INDEX_CHARMING ?>
            </p>                        
        </div>
        <div class="right-core-div">
        	<p class="core-p">
            	<img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_CORE_VALUE ?>">
                <?php echo _INDEX_REBORN ?>
            </p>
        	<p class="core-p">
            	<img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_CORE_VALUE ?>">
                <?php echo _INDEX_INTIMATE ?>
            </p>            
        	<p class="core-p">
            	<img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_CORE_VALUE ?>">
                <?php echo _INDEX_TRANSFORM ?>
            </p>
        	<p class="core-p">
            	<img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_CORE_VALUE ?>">
                <?php echo _INDEX_SECURE ?>
            </p>        
        </div>    	
    </div>
    <div class="bottom-right-flower-div">
        <img src="img/bottom-right-flower.png" class="bottom-right-flower" alt="Samofa 莎魔髪" title="Samofa 莎魔髪">
    </div>
</div>
<div class="clear"></div>
<div class="width100 same-padding text-center company-div">
	<h1 class="dark-pink-text hi-title contact-title big-header-color"><?php echo _INDEX_COMPANY_PURPOSE ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="three-div-width text-center contact-three-div">
        
            <img src="img/wealth.png" alt="<?php echo _INDEX_BRINGING_WEALTH ?>" title="<?php echo _INDEX_BRINGING_WEALTH ?>" class="contact-icon">
            <p class="contact-p dark-pink-text company-p"><?php echo _INDEX_BRINGING_WEALTH ?></p>
        
    </div>
	<div class="three-div-width mid-three-div-width-margin text-center contact-three-div mid-contact-three-div">
    	<img src="img/better.png" alt="<?php echo _INDEX_BETTER_COMMUNITY ?>" title="<?php echo _INDEX_BETTER_COMMUNITY ?>" class="contact-icon">
        <p class="contact-p dark-pink-text company-p"><?php echo _INDEX_BETTER_COMMUNITY ?></p>    
    </div>    
	<div class="three-div-width text-center contact-three-div">
    	
            <img src="img/talent.png" alt="<?php echo _INDEX_TALENT ?>" title="<?php echo _INDEX_TALENT ?>" class="contact-icon">
            <p class="contact-p dark-pink-text company-p"><?php echo _INDEX_TALENT ?></p>     
   
    </div>    
</div>
<div class="clear"></div>
<div class="width100 same-padding service-div">
	<h1 class="dark-pink-text hi-title contact-title big-header-color text-center"><?php echo _INDEX_SERVICES ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="left-service-div">
    	<img src="img/our-services.png" class="width100" alt="<?php echo _INDEX_SAMOFA ?>">
    </div>
    <div class="right-service-div">
    	<table class="transparent-table dark-pink-text-table">
        	<tbody>
            	<tr>
                	<td><img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_SERVICES ?>"></td>
                    <td><?php echo _INDEX_VALUABLE_LIFE ?></td>
                </tr>
            	<tr>
                	<td><img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_SERVICES ?>"></td>
                    <td><?php echo _INDEX_CONSIDERABLE ?></td>
                </tr>                
            	<tr>
                	<td><img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_SERVICES ?>"></td>
                    <td><?php echo _INDEX_TEAM_TRAINING ?></td>
                </tr> 
            	<tr>
                	<td><img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_SERVICES ?>"></td>
                    <td><?php echo _INDEX_LEADERSHIP ?></td>
                </tr>                
            	<tr>
                	<td><img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_SERVICES ?>"></td>
                    <td><?php echo _INDEX_HEALTHY_BODY ?></td>
                </tr>      
            	<tr>
                	<td><img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_SERVICES ?>"></td>
                    <td><?php echo _INDEX_BUILD_CONFIDENCE ?></td>
                </tr>                 
            	<tr>
                	<td><img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_SERVICES ?>"></td>
                    <td><?php echo _INDEX_IMPROVE_QUALITY ?></td>
                </tr>   
            	<tr>
                	<td><img src="img/rose.png" class="rose-png" alt="<?php echo _INDEX_SERVICES ?>"></td>
                    <td><?php echo _INDEX_MORE_FREEDOM ?></td>
                </tr>                                                      
        	</tbody>
        </table>
    </div>
</div>
<div class="clear"></div>
<div class="width100 same-padding text-center quote-big-div contact-big-div two-four-div">
	<h1 class="dark-pink-text hi-title contact-title big-header-color"><?php echo _INDEX_LEARN_MORE2 ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <a href="pdf/About-Samofa.pdf" download target="_blank">
    	<div class="four-div opacity-hover pointer">
        	<img src="img/samofa-square.png" class="four-div-img">
            <p  class="contact-p dark-pink-text index-box-p"><?php echo _INDEX_ABOUT_US2 ?> <img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></p>
        </div>
    </a>
    <a href="pdf/Magic-Bloca.pdf" download target="_blank">
    	<div class="four-div opacity-hover pointer second-four-div four-mid">
        	<img src="img/iconz3.png" class="four-div-img">
            <p  class="contact-p dark-pink-text index-box-p">Magic BloCA<sup class="dark-pink-text smaller-h1">TM</sup> 脱糖宝 <img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></p>
        </div>
    </a>
    <a href="pdf/purifying-balancing-shampoo.pdf" download target="_blank">
    	<div class="four-div opacity-hover pointer third-four-div">
        	<img src="img/iconz2.png" class="four-div-img">
            <p  class="contact-p dark-pink-text index-box-p"><?php echo _INDEX_PURI ?> <img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></p>
        </div>
    </a>
    <a href="pdf/serum-ultimate-elixir.pdf" download target="_blank">
    	<div class="four-div opacity-hover pointer forth-div">
        	<img src="img/iconz1.png" class="four-div-img">
            <p  class="contact-p dark-pink-text index-box-p"><?php echo _INDEX_SERUM2 ?> <img src="img/download2.png" class="download-icon" alt="<?php echo _INDEX_DOWNLOAD ?>" title="<?php echo _INDEX_DOWNLOAD ?>"></p>
        </div>
    </a>            
</div>
<div class="clear"></div>
<div class="width100 same-padding text-center quote-big-div">
	<h1 class="dark-pink-text hi-title contact-title big-header-color"><?php echo _INDEX_INTERESTED_IN_US ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
	<div class="three-div-width text-center contact-three-div">
        <a href="tel:+60124616966">
            <img src="img/call.png" alt="<?php echo _INDEX_CONTACT_NO ?>" title="<?php echo _INDEX_CONTACT_NO ?>" class="contact-icon">
            <p class="contact-p dark-pink-text">012-461 6966</p>
        </a>
    </div>
	<div class="three-div-width mid-three-div-width-margin text-center contact-three-div mid-contact-three-div">
    	<img src="img/email.png" alt="<?php echo _INDEX_EMAIL ?>" title="<?php echo _INDEX_EMAIL ?>" class="contact-icon">
        <p class="contact-p dark-pink-text">samofa.official@gmail.com</p>    
    </div>    
	<div class="three-div-width text-center contact-three-div">
    	<a href="https://www.facebook.com/SAMOFA.Official/" class="opacity-hover" target="_blank">
            <img src="img/facebook.png" alt="Facebook" title="Facebook" class="contact-icon">
            <p class="contact-p dark-pink-text">SAMOFA.Official</p>     
        </a>
    </div>    
</div>


<style>
.fb-video iframe {
    margin-top: -220px;
	width: 100%;
}
@media all and (max-width: 1770px){
.fb-video iframe {
	margin-top: -190px;
}	
	
}
@media all and (max-width: 1550px){
.fb-video iframe {
    margin-top: -170px;
}	
}
@media all and (max-width: 1380px){
.fb-video iframe {
    margin-top: -150px;
}		
}
@media all and (max-width: 1230px){
.fb-video iframe {
    margin-top: -130px;
}		
}
@media all and (max-width: 1050px){
.fb-video iframe {
    margin-top: -130px;
}
}
@media all and (max-width: 800px){
.fb-video iframe {
    margin-top: 0;
}
}
</style>
<?php include 'js.php'; ?>
</body>
</html>