<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Shipping.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$date = date("Y-m-d");
$time = date("h:i a");

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $id = $orderUid;

    $name = rewrite($_POST["insert_name"]);
    $_SESSION['name'] = $name;
    $contactNo = rewrite($_POST["insert_contactNo"]);
    $_SESSION['contact'] = $contactNo;
    $address_1 = rewrite($_POST["insert_address_1"]);
    $address_2 = rewrite($_POST["insert_address_2"]);
    $city = rewrite($_POST["insert_city"]);
    $zipcode = rewrite($_POST["insert_zipcode"]);
    $state = rewrite($_POST["insert_state"]);
    $country = rewrite($_POST["insert_country"]);
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$orderDetails = $userOrder[0];

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
// $rank= $userDetails->getStatus();
$rank= $userDetails->getRank();

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
{
    $productListHtml = getShoppingCart($conn,$rank,2);
}
else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <!-- <meta property="og:title" content="User Dashboard | De Xin Guo Ji 德鑫国际" /> -->
    <meta property="og:title" content="Payment Method | Samofa 莎魔髪" />
    <title>Payment Method | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
    <?php include 'css.php'; ?> 
</head>

<body class="body">

<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-menu-distance75 same-padding">

    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _SHIPPING_PAYMENT ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

        <form method="POST" action="utilities/orderInformation.php" enctype="multipart/form-data">
        <!-- <form method="POST" action="#" enctype="multipart/form-data"> -->

        <p class="info-title dark-pink-text"><b><?php echo _SHIPPING_SHIP_TO ?></b></p>

        <input class="clean white-input two-box-input" type="hidden" id="order_id" name="order_id" value="<?php echo $id;?>">
        <!-- <input class="clean white-input two-box-input" type="hidden" id="insert_username" name="insert_username" value="<?php echo $username;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankname" name="insert_bankname" value="<?php echo $bankName;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountholder" name="insert_bankaccountholder" value="<?php echo $bankAccountHolder;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountnumber" name="insert_bankaccountnumber" value="<?php echo $bankAccountNo;?>"> -->
        <input class="clean white-input two-box-input" type="hidden" id="insert_name" name="insert_name" value="<?php echo $name;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_contactNo" name="insert_contactNo" value="<?php echo $contactNo;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_address_1" name="insert_address_1" value="<?php echo $address_1;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_address_2" name="insert_address_2" value="<?php echo $address_2;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_city" name="insert_city" value="<?php echo $city;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_zipcode" name="insert_zipcode" value="<?php echo $zipcode;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_state" name="insert_state" value="<?php echo $state;?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_country" name="insert_country" value="<?php echo $country;?>">

        <p class="info-title dark-pink-text"><b><?php echo _SHIPPING_PAYMENT ?></b></p>
        <p class="smaller-text pink-text"><?php echo _SHIPPING_ALL_TRANS ?></p>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _SHIPPING_BANK ?></p>
            <p class="fake-input-p">Maybank</p>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _SHIPPING_ACC_NO ?></p>
            <p class="fake-input-p">XXXXXXXXXXXXXX</p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _SHIPPING_BANK_HOLDER ?></p>
            <p class="fake-input-p">Samofa</p>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _SHIPPING_PAYMENT_METHOD ?></p>
            <select class="clean de-input" type="text" id="payment_method" name="payment_method">
            <option value=" - " name=" - "><?php echo _SHIPPING_SELECT_PAYMENT_METHOD ?></option>
            <option id="billPlz" value="<?php echo 'CreateABill.php?amount='.$_SESSION['total'] ?>" name="Online Banking">Online Banking</option>
            <option id="cdm" value="CDM" name="CDM">CDM (Cash Deposit Machine)</option>
            </select>
        </div>

        <div class="clear"></div>               

        <div style="display: none" id="showLater">
            <div class="dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_AMOUNT ?></p>
                <input class="clean de-input" required type="text" placeholder="0.00"  id="payment_amount" name="payment_amount">
            </div>        

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_BANK_REFERENCE ?></p>
                <input class="clean de-input" required type="text" placeholder="<?php echo _SHIPPING_BANK_REFERENCE ?>"  id="payment_bankreference" name="payment_bankreference">
            </div>

            <div class="clear"></div>     

            <div class="dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_BANK_IN_DATE ?></p>
                <input class="clean de-input" required type="date" id="payment_date" name="payment_date" value="<?php echo $date ?>">
            </div>        

            <div class="dual-input second-dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_BANK_IN_TIME ?></p>
                <input class="clean de-input"  required type="time" id="payment_time" name="payment_time" value="<?php echo $time ?>">
            </div>

            <div class="clear"></div>  

            <div class="dual-input">
                <p class="input-top-text"><?php echo _SHIPPING_UPLOAD_RECEIPT ?></p>
                <input class="hidden-input" type="file" name="file" />
            </div>  
        </div>

        <div class="clear"></div>   

        <input type="hidden" value="PENDING" class="clean edit-profile-input payment-input" id="payment_status" name="payment_status">
        
        <div class="clear"></div> 

        <div class="width100 top-bottom-spacing">
            <table class="table-css">
                <thead>
                    <th><?php echo _HEADERBEFORELOGIN_PRODUCT ?></th>
                    <th><?php echo _VIEW_CART_QUANTITY ?></th>
                    <th><?php echo _PRODUCTDETAILS_NAME ?></th>
                    <th><?php echo _PRODUCTDETAILS_PRICE ?></th>
                </thead>
                <tbody>
                    <?php echo $productListHtml; ?>
                </tbody>
            </table>
        </div>

        <div class="clear"></div>

        <div class="width100 text-center">
            <!-- <div class="width100 text-center top-bottom-spacing">
            <button class="dark-pink-button border0 clean black-button add-to-cart-btn checkout-btn continue2 add-to-cart-btn2" name="register"><?php echo _CHECKOUT_NEXT ?></button>
            </div> -->

            <div class="width100 text-center top-bottom-spacing">
                <button class="dark-pink-button border0 clean black-button add-to-cart-btn" name="register"><?php echo _CHECKOUT_NEXT ?></button>
            </div>

            <div class="clear"></div>

            <div class="width100 text-center margin-bottom30">
                <p class="continue-shopping pointer continue2"><a href="checkout.php" class="pink-text opacity-hover back-text"><img src="img/back.png" class="checkout-back-btn" alt="<?php echo _VIEW_CART_BACK ?>" title="<?php echo _VIEW_CART_BACK ?>" > <?php echo _VIEW_CART_BACK ?></a></p>
            </div>                    
        </div>

    </form>

</div>

<?php include 'js.php'; ?>

<script>
function la(src)
{
    window.location=src;
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
    $("#payment_method").change(function(){
        if ($("#payment_method").val() == 'CDM') {
        $("#showLater").slideDown(function(){
            $(this).show();
        });
        }
        else {
        $("#showLater").slideUp(function(){
            $(this).hide();
        });
        }
    });

    $("#completeOrder").click(function(){
        var sessName = '<?php echo 'CreateABill.php?amount='.$_SESSION['total'] ?>';
        var sessName2 = '<?php echo 'CreateABill.php' ?>';
        if ($("#payment_method").val() == 'CDM') {
        window.location = sessName2;
        }
        else {

            window.location = sessName;
        }
        });
    });
</script>

</body>
</html>