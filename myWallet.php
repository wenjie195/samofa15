<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="My Wallet | Samofa 莎魔髪" />
    <title>My Wallet | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _MEMBER_WITHDRAWAL ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <div class="width100 same-padding text-center">
    	<div class="dashboard-box center-div">
            <img src="img/icon4.png" alt="<?php echo _USERDASHBOARD_AVAILABLE_BALANCE ?>" title="<?php echo _USERDASHBOARD_AVAILABLE_BALANCE ?>">
            <p class="box-p min-height-auto"><b><?php echo _USERDASHBOARD_AVAILABLE_BALANCE ?></b></p>
            <p class="box-p min-height-auto">RM<?php echo $userData->getAmount();?></p>
        </div>   
    </div>    

    <div class="clear"></div>

    <?php 
        $bankStatus = $userData->getBankName();
        $bankAccHolderStatus = $userData->getBankAccHolder();
        $bankAccStatus = $userData->getBankAccNo();
        if($bankStatus == "")
        {
        ?>
            <div class="width100 overflow same-padding text-center">
                <p>
                    <a href="editProfile.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">
                        <?php echo _MEMBER_BANK_DETAILS_MSG ?>
                    </a>
                </p>
            </div>
        <?php
        }
        elseif($bankAccHolderStatus == "")
        {
        ?>
            <div class="width100 overflow same-padding text-center">
                <p>
                    <a href="editProfile.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">
                        <?php echo _MEMBER_BANK_DETAILS_MSG ?>
                    </a>
                </p>
            </div>
        <?php
        }
        elseif($bankAccStatus == "")
        {
        ?>
            <div class="width100 overflow same-padding text-center">
                <p>
                    <a href="editProfile.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">
                        <?php echo _MEMBER_BANK_DETAILS_MSG ?>
                    </a>
                </p>
            </div>
        <?php
        }
        // elseif($bankAccHolderStatus != "" && $bankAccStatus != "")
        elseif($bankStatus != "" && $bankAccHolderStatus != "" && $bankAccStatus != "")
        {
        ?>

            <div class="width100 overflow same-padding">
                <form action="utilities/submitWithdrawalFunction.php" method="POST">
                        <div class="dual-input">
                            <p class="input-top-text"><?php echo _EDITPROFILE_BANK_NAME ?></p>
                            <input class="clean de-input" type="text" placeholder="<?php echo _EDITPROFILE_BANK_NAME ?>" value="<?php echo $userData->getBankName();?>" id="bank_name" name="bank_name" readonly>
                        </div>    
                        <div class="dual-input second-dual-input">  
                            <p class="input-top-text"><?php echo _EDITPROFILE_BANK_ACC_HOLDER ?></p>
                            <input class="clean de-input" type="text" placeholder="<?php echo _EDITPROFILE_BANK_ACC_HOLDER ?>" value="<?php echo $userData->getBankAccHolder();?>" id="bank_acc_holder" name="bank_acc_holder" readonly>
                        </div>
                        <div class="clear"></div>
                        <div class="dual-input">
                            <p class="input-top-text"><?php echo _EDITPROFILE_BANK_ACC_NO ?></p>
                            <input class="clean de-input" type="text" placeholder="<?php echo _EDITPROFILE_BANK_ACC_NO ?>" value="<?php echo $userData->getBankAccNo();?>" id="bank_account_no" name="bank_account_no" readonly>
                        </div>
                        <div class="dual-input second-dual-input">  
                            <p class="input-top-text"><?php echo _MEMBER_WITHDRAWAL_AMOUNT ?></p>
                            <input class="clean de-input" type="text" placeholder="<?php echo _MEMBER_WITHDRAWAL_AMOUNT ?>"  id="withdrawal_amount" name="withdrawal_amount" required>
                        </div>
                        <div class="clear"></div>
                        <div class="dual-input">
                            <p class="input-top-text"><?php echo _EPIN ?></p>
                            <input class="clean de-input" type="text" placeholder="<?php echo _EPIN ?>"  id="user_epin" name="user_epin" required>
                        </div>
                    </div>

                    <div class="clear"></div>

                    <div class="width100 same-padding text-center top-bottom-distance">
                        <button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
                    </div>
                </form>
            </div>

        <?php
        }
    ?>

</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>
