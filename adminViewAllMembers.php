<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allUser = getUser($conn, " WHERE user_type = ? ", array("user_type"), array(1), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://dxforextrade88.com/userDashboard.php" /> -->
    <meta property="og:title" content="Edit Member Profile | Samofa 莎魔髪" />
    <title>Edit Member Profile | Samofa 莎魔髪</title>
    <!-- <link rel="canonical" href="https://dxforextrade88.com/userDashboard.php" /> -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<?php include 'css.php'; ?>
    
</head>

<body class="body">
<?php include 'headerAfterLogin.php'; ?>

<div class="width100 menu-distance75 min-height-with-flower">
    <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _ADMIN_EDIT_MEMBER_PROFILE ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>
    <div class="width100 overflow same-padding margin-bottom30">
    	<div class="search-input-div">
    		<img src="img/search.png" class="search-png" alt="<?php echo _ADMIN_SEARCH ?>" title="<?php echo _ADMIN_SEARCH ?>">
            <input class="clean fake-line-input" type="text" placeholder="<?php echo _ADMIN_SEARCH ?> <?php echo _JS_USERNAME ?>" id="myInput" onkeyup="myFunction()">
        </div>
    </div>
    <div class="width100 same-padding container-div1">

    <div class="overflow-scroll-div">
        <table class="table-css" id="myTable">
            <thead>
                <tr>
                    <th><?php echo _PRODUCTDETAILS_NO ?></th>
                    <th><?php echo _JS_USERNAME ?></th>
                    <th><?php echo _JS_EMAIL ?></th>
                    <th><?php echo _JS_PHONE ?></th>
                    <th><?php echo _ADMIN_CURRENT_CREDIT ?></th>
                    <th><?php echo _PRODUCTDETAILS_STATUS ?></th>
                    <th><?php echo _EDITPROFILE ?></th>
                    <th><?php echo _ADMIN_EDIT_PASSWORD_EPIN ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($allUser)
                {
                    for($cnt = 0;$cnt < count($allUser) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $allUser[$cnt]->getUsername();?></td>
                            <td><?php echo $allUser[$cnt]->getEmail();?></td>
                            <td><?php echo $allUser[$cnt]->getPhoneNo();?></td>
                            <td><?php echo $allUser[$cnt]->getAmount();?></td>
                            <td><?php echo $allUser[$cnt]->getStatus();?></td>
                            <td>
                                <form method="POST" action="adminEditUserProfile.php" class="hover1">
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="user_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                        <img src="img/edit.png" class="edit-icon1 hover1a" alt="<?php echo _EDITPROFILE ?>" title="<?php echo _EDITPROFILE ?>">
                                        <img src="img/edit2.png" class="edit-icon1 hover1b" alt="<?php echo _EDITPROFILE ?>" title="<?php echo _EDITPROFILE ?>">
                                    </button>
                                </form>                  
                            </td>
                            <td>
                                <form method="POST" action="adminEditUserPasswordEpin.php" class="hover1">
                                    <button class="clean hover1 transparent-button pointer" type="submit" name="user_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                        <img src="img/edit.png" class="edit-icon1 hover1a" alt="<?php echo _ADMIN_EDIT_PASSWORD_EPIN ?>" title="<?php echo _ADMIN_EDIT_PASSWORD_EPIN ?>">
                                        <img src="img/edit2.png" class="edit-icon1 hover1b" alt="<?php echo _ADMIN_EDIT_PASSWORD_EPIN ?>" title="<?php echo _ADMIN_EDIT_PASSWORD_EPIN ?>">
                                    </button>
                                </form>                  
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?> 
            </tbody>
        </table>
    </div>

    </div>
</div>

<div class="clear"></div>

<img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>