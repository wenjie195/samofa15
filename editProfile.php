<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://dxforextrade88.com/" />-->
    <meta property="og:title" content="Edit Profile | Samofa 莎魔髪" />
    <title>Edit Profile | Samofa 莎魔髪</title>
    <!--<link rel="canonical" href="https://dxforextrade88.com/" />-->
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'headerAfterLogin.php'; ?>
<div class="width100 same-padding menu-distance75">

   <h1 class="dark-pink-text hi-title contact-title text-center modal-h1 big-header-color"><?php echo _EDITPROFILE ?><img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

 	<form action="utilities/editProfileFunction.php" method="POST">
	<p class="dark-pink-text section-p"><?php echo _MEMBER_ACC_DETAILS ?></p>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" value="<?php echo $userData->getUsername();?>" id="update_username" name="update_username" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
        	<select class="clean de-input" id="register_country" name="register_country" required>
                        <?php
                            {
                                for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                                {
                                    if ($userData->getCountry() == $countryList[$cntPro]->getEnName())
                                    {
                                    ?>
                                        <option selected value="<?php echo $countryList[$cntPro]->getEnName(); ?>"> 
                                            <?php echo $countryList[$cntPro]->getEnName(); ?>
                                        </option>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                        <option value="<?php echo $countryList[$cntPro]->getEnName(); ?>"> 
                                            <?php echo $countryList[$cntPro]->getEnName(); ?>
                                        </option>
                                    <?php
                                    }
                                }
                            }
                        ?>
            </select>
        </div>
        <div class="clear"></div>
		
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_EMAIL ?>" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email" required>
        </div>

        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _JS_PHONE ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_PHONE ?>" value="<?php echo $userData->getPhoneNo();?>" id="update_phone" name="update_phone" required>
        </div>

        <div class="clear"></div>
		<p class="dark-pink-text section-p"><?php echo _MEMBER_BANK_DETAILS ?></p>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _EDITPROFILE_BANK_NAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _EDITPROFILE_BANK_NAME ?>" value="<?php echo $userData->getBankName();?>" id="update_bank_name" name="update_bank_name" required>
        </div>

        <div class="dual-input second-dual-input">
        	<p class="input-top-text"><?php echo _EDITPROFILE_BANK_ACC_HOLDER ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _EDITPROFILE_BANK_ACC_HOLDER ?>" value="<?php echo $userData->getBankAccHolder();?>" id="update_bank_account_holder" name="update_bank_account_holder" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _EDITPROFILE_BANK_ACC_NO ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _EDITPROFILE_BANK_ACC_NO ?>" value="<?php echo $userData->getBankAccNo();?>" id="update_bank_account_no" name="update_bank_account_no" required>
        </div>

        <div class="clear"></div>
		<p class="dark-pink-text section-p"><?php echo _MEMBER_DELIVERY_ADDRESS ?></p>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_FIRSTNAME ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _JS_FIRSTNAME ?>" value="<?php echo $userData->getFirstname();?>" id="update_firstname" name="update_firstname" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _JS_LASTNAME ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _JS_LASTNAME ?>" value="<?php echo $userData->getLastname();?>" id="update_lastname" name="update_lastname" required>
        </div>
        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _MEMBER_RECEIVER_EMAIL ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _MEMBER_RECEIVER_EMAIL ?>" value="" id="" name="" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _MEMBER_RECEIVER_PHONE_NO ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _MEMBER_RECEIVER_PHONE_NO ?>" value=""  required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_ADDRESS1 ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _CHECKOUT_ADDRESS1 ?>" value="" id="" name="" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_ADDRESS2 ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _CHECKOUT_ADDRESS2 ?>" value=""  required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_CITY ?></p>
            <input class="clean de-input" type="text" placeholder="<?php echo _CHECKOUT_CITY ?>" value="" id="" name="" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_ZIP_CODE ?></p>
        	<input class="clean de-input" type="text" placeholder="<?php echo _CHECKOUT_ZIP_CODE ?>" value=""  required>
        </div>
        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-text"><?php echo _JS_COUNTRY ?></p>
        	<select class="clean de-input" id="" name="" required>
                        <?php
                            {
                                for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                                {
                                    if ($userData->getCountry() == $countryList[$cntPro]->getEnName())
                                    {
                                    ?>
                                        <option selected value="<?php echo $countryList[$cntPro]->getEnName(); ?>"> 
                                            <?php echo $countryList[$cntPro]->getEnName(); ?>
                                        </option>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                        <option value="<?php echo $countryList[$cntPro]->getEnName(); ?>"> 
                                            <?php echo $countryList[$cntPro]->getEnName(); ?>
                                        </option>
                                    <?php
                                    }
                                }
                            }
                        ?>
            </select>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _CHECKOUT_STATE ?></p>
        	<select class="clean de-input" id="" name="" required>
            	<option></option>
            </select>
        </div>        
        <div class="clear"></div>        
        <div class="width100 text-center top-bottom-distance">
        	<button class="clean button-width transparent-button dark-pink-button" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>

    </form>

</div>

	<div class="clear"></div>
    <img src="img/flower2.png" alt="<?php echo _JS_FLOWER ?>" title="<?php echo _JS_FLOWER ?>" class="flower-img">
    <div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>