<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
    <h1 class="dark-pink-text hi-title text-center modal-h1 big-header-color margin-top40"><?php echo _USERDASHBOARD_DOWNLINE ?> <img src="img/feather.png" class="feather-png" alt="<?php echo _INDEX_SAMOFA ?>"></h1>

    <?php $level = $userLevel->getCurrentLevel();?>

    <div class="overflow-scroll-div same-padding">
        <table class="table-css">
            <thead>
                <tr>
                    <th><?php echo _USERDASHBOARD_GEN ?></th>
                    <th><?php echo _USERDASHBOARD_NAME ?></th>
                    <th>ID</th>
                    <th><?php echo _USERDASHBOARD_SPONSOR ?></th>
                    <th><?php echo _USERDASHBOARD_LAST_ORDER ?></th>
                    <th><?php echo _USERDASHBOARD_RANK ?></th>
                    <th><?php echo _JS_COUNTRY ?></th>
                    <th><?php echo _USERDASHBOARD_STATUS ?></th>
                    <th><?php echo _USERDASHBOARD_SALES_VALUE ?></th>
                    <th><?php echo _USERDASHBOARD_HIERARCHY ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($getWho)
                {
                    for($cnt = 0;$cnt < count($getWho) ;$cnt++)
                    {
                    ?>
                    <tr>
                        <td>
                            <?php 
                                $downlineLvl = $getWho[$cnt]->getCurrentLevel();
                                echo $lvl = $downlineLvl - $level;
                            ?>
                        </td>

                        <td>
                            <?php
                                $userUid = $getWho[$cnt]->getReferralId();

                                $thisUserDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
                                echo $username = $thisUserDetails[0]->getUsername();
                            ?>
                        </td>

                        <td><?php echo $userMemberID = $thisUserDetails[0]->getMemberID();?></td>

                        <td>
                            <?php
                                $uplineUid = $getWho[$cnt]->getReferrerId();

                                $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                                // $userData = $uplineDetails[0];

                                echo $uplineUsername = $uplineDetails[0]->getUsername();

                            ?>
                        </td>

                        <td><?php echo date("d-m-Y",strtotime($thisUserDetails[0]->getDateUpdated()));?></td>

                        <td>
                            <?php 
                                $rank = $thisUserDetails[0]->getRank();
                                if($rank != '')
                                {
                                    echo $rank;
                                }
                                else
                                {
                                    echo "non member";
                                }
                            ?>
                        </td>
                        <td><?php echo $country = $thisUserDetails[0]->getCountry();?></td>
                        <td><?php echo $status = $thisUserDetails[0]->getStatus();?></td>
                        <td><?php echo $salesValue = $thisUserDetails[0]->getSalesValue();?></td>
                        <!-- <td>Tree View</td> -->

                        <td>
                            <form action="userDownlineDetails.php" method="POST">
                                <button class="clean pink-button view-btn" type="submit" name="user_uid" value="<?php echo $getWho[$cnt]->getReferralId();?>"><?php echo _USERDASHBOARD_VIEW ?>

                                </button>
                            </form>
                        </td>

                    </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>

    <div class="clear"></div>
</body>
</html>