	<div class="width100 same-padding container-div1">
    	<!-- if achieved, add the class achieved besides seven-div-->
		<div class="seven-div achieved">
        	<img src="img/start.png" class="milestone-img" alt="<?php echo _USERDASHBOARD_START ?>" title="<?php echo _USERDASHBOARD_START ?>">
        	<div class="circle-div"></div>
            <div class="circle-div2"></div>
            <p><?php echo _USERDASHBOARD_START ?></p>
        </div>

        <div class="seven-div achieved">
        	<img src="img/cert.png" class="milestone-img" alt="<?php echo _USERDASHBOARD_FREE_CERT_SCHOLARSHIP ?>" title="<?php echo _USERDASHBOARD_FREE_CERT_SCHOLARSHIP ?>">
        	<div class="circle-div"></div>
            <div class="circle-div2"></div>
            <p><?php echo _USERDASHBOARD_FREE_CERT_SCHOLARSHIP ?></p>
        </div> 

		<div class="seven-div achieved">
        	<img src="img/personal-trip.png" class="milestone-img" alt="<?php echo _USERDASHBOARD_FREE_TRIP ?>" title="<?php echo _USERDASHBOARD_FREE_TRIP ?>">
        	<div class="circle-div"></div>
            <div class="circle-div2"></div>
            <p><?php echo _USERDASHBOARD_FREE_TRIP ?></p>
        </div>    
		<div class="seven-div achieved">
        	<img src="img/group-trip.png" class="milestone-img" alt="<?php echo _USERDASHBOARD_FREE_GROUP_TRIP ?>" title="<?php echo _USERDASHBOARD_FREE_GROUP_TRIP ?>">
        	<div class="circle-div"></div>
            <div class="circle-div2"></div>
            <p><?php echo _USERDASHBOARD_FREE_GROUP_TRIP ?></p>
        </div>       
        

		<div class="seven-div">
        	<img src="img/scholarship.png" class="milestone-img" alt="<?php echo _USERDASHBOARD_FREE_SCHOLARSHIP ?>" title="<?php echo _USERDASHBOARD_FREE_SCHOLARSHIP ?>">
        	<div class="circle-div"></div>
            <div class="circle-div2"></div>
            <p><?php echo _USERDASHBOARD_FREE_SCHOLARSHIP ?></p>
        </div> 
		  
		<div class="seven-div">
        	<img src="img/new-car.png" class="milestone-img" alt="<?php echo _USERDASHBOARD_UNLOCKING_NEW_CAR ?>" title="<?php echo _USERDASHBOARD_UNLOCKING_NEW_CAR ?>">
        	<div class="circle-div"></div>
            <div class="circle-div2"></div>
            <p><?php echo _USERDASHBOARD_UNLOCKING_NEW_CAR ?></p>
        </div> 
		<div class="seven-div last-seven-div">
        	<img src="img/house.png" class="milestone-img" alt="<?php echo _USERDASHBOARD_UNLOCKING_SPECIAL_HOUSE_UNIT ?>" title="<?php echo _USERDASHBOARD_UNLOCKING_SPECIAL_HOUSE_UNIT ?>">
        	<div class="circle-div"></div>
            <div class="circle-div2"></div>
            <p><?php echo _USERDASHBOARD_UNLOCKING_SPECIAL_HOUSE_UNIT ?></p>
        </div>                            
    </div>